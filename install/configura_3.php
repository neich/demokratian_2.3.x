<?php
require_once("../config/config.inc.php");
require_once("../inc_web/conexion.php");
include("../basicos_php/basico.php");

$user = fn_filtro($con, $_POST['user']);
$email = fn_filtro($con, $_POST['email']);
$pass = fn_filtro($con, $_POST['pass']);
$pass2 = fn_filtro($con, $_POST['pass2']);
$username = fn_filtro($con, $_POST['username']);
$nif = fn_filtro($con, $_POST['nif']);
$ides = explode("#", $_POST['provincia']);
$provincia = fn_filtro($con, $ides[0]);
$id_ccaa = fn_filtro($con, $ides[1]);
$id_municipio = fn_filtro($con, $_POST['id_municipio']);
$autenticacion_solo_local = $_POST['autenticacion_solo_local'];
$dir_simplesaml = $_POST['dir_simplesaml'];
$crear_usuarios_automaticamente = $_POST['crear_usuarios_automaticamente'];
$tipo_usuario = fn_filtro($con, $_POST['tipo_usuario']);


function show_error($error) {
    echo "ERROR#" . $error;
    die;
}

function show_ok($msg) {
    echo "OK#" . $msg;
    die;
}

function comprobar_nombre_usuario($nombre_usuario) {
    //compruebo que el tamaño del string sea válido. 
    if (strlen($nombre_usuario) < 4 || strlen($nombre_usuario) > 21) {

        $error = "error1";
        return $error;
    }

    //compruebo que los caracteres sean los permitidos 
    $permitidos = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_";
    for ($i = 0; $i < strlen($nombre_usuario); $i++) {
        if (strpos($permitidos, substr($nombre_usuario, $i, 1)) === false) {
            $error = "error2";
            return $error;
        }
    }
    //echo $nombre_usuario . " es válido<br>"; 
    return $nombre_usuario;
}

if ($autenticacion_solo_local == 'false' && !file_exists($dir_simplesaml . "/lib/_autoload.php")) {
    show_error("La instalación de simplesamlphp no es correcta (" . $dir_simplesaml . ") ");
}

$nombre_usuario_new = comprobar_nombre_usuario($user);

if ($nombre_usuario_new == "error1") {
    $errores = "El nombre de usuario" . $nombre_usuario . " no es válido<br/> Tiene que tener entre 5 y 20 caracteres <br/>";


    //return false;
} elseif ($nombre_usuario_new == "error2") {
    $errores = "El nombre de usuario" . $nombre_usuario . " no es válido, esta mal formado, no puede tener espacios en blanco, acentos, ñ ...<br/>";
} else {

    if ($pass != $pass2) {
        $errores = "Hay un error, los password eran distintos.";
    } else {


        /// miramos si ese nombre de usuario se esta usando
        $result_usu = mysqli_query($con, "SELECT usuario FROM $tbn9 WHERE usuario='$nombre_usuario_new'") or die("No se pudo realizar la consulta a la Base de datos");
        $quants_usu = mysqli_num_rows($result_usu);

        if ($quants_usu == "") {

            $passw = md5($pass);
            $nivel_usuario = 2; // nivel de administrador
            $nivel_acceso = 0; //maximo nivel de administrador
            $tipo_votante = 1; //usuario tipo afiliado
            
            $insql = "insert into $tbn9 (pass,id_provincia,usuario,correo_usuario,tipo_votante,nivel_acceso,nivel_usuario,nombre_usuario,id_ccaa,nif,id_municipio) values (  \"$passw\",  \"$provincia\", \"$nombre_usuario_new\", \"$email\", \"$tipo_votante\", \"$nivel_acceso\", \"$nivel_usuario\", \"$username\",\"$id_ccaa\",\"$nif\", \" $id_municipio\")";
            mysqli_query($con, $insql);
            if (mysqli_error($con)) {
                $errores = "Error al incluir datos: " . mysqli_error($con);
            } else {
                $correcto = " Sus datos han sido incluidos <br/> Ya puede acceder al sistema de votaciones";
            }
        } else {
            $errores = "Este nombre de usuario ya se esta usando, cambielo, por favor";
        }
    }
}



if ($errores != "") {
    show_error($errores . " ");
}


$file = "../config/config.inc.php"; //archivo que hay que modificar					
$fh = fopen($file, 'r+');
$string1 = "
###################################################################################################################################
##########                                               Autenticacion                                                   ##########  
###################################################################################################################################								
\$cfg_autenticacion_solo_local=" . $autenticacion_solo_local . ";    // ¿Permitimos autenticacion federada? cfg_autenticacion_solo_local==false -> SI
\$cfg_dir_simplesaml=\"" . $dir_simplesaml . "\";                    // Directorio donde hemos instalado simplesamlphp en modo SP.
\$cfg_crear_usuarios_automaticamente=" . $crear_usuarios_automaticamente . ";    // Cuando tenemos autenticacion federada, ¿creamos usuarios automaticamente?
\$cfg_tipo_usuario=" . $tipo_usuario . ";    // Al crear automaticamente, el tipo por defecto

?>";

if ($autenticacion_solo_local == 'false') {
    $string1 .= "if ( \$cfg_autenticacion_solo_local == false ) { require_once(\"" . $dir_simplesaml . "/lib/_autoload.php\"); } \n?>";
}


fseek($fh, -3, SEEK_END); // nos vamos 3 caraceres antes para quitar el cierre de php
fwrite($fh, $string1) or show_error("Could not write file!");
fclose($fh);  //  Cerramos el fichero 

show_ok("<div class=\"alert alert-success\"> 
              <a class=\"close\" data-dismiss=\"alert\">x</a>" . $correcto . " Se han guardado los datos correctamente
</div>");
?>