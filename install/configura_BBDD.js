// JavaScript Document
/*
 Jquery Validation using jqBootstrapValidation
 example is taken from jqBootstrapValidation docs 
 */
// $('#contenido').on('submit','#formulario',function(event){ 
$(function() {
    $("#FormBBDD").find("input,select").jqBootstrapValidation(// este seria con un formulario con class="form-horizontal"
            //$("input#name,input#email,select#provincia").jqBootstrapValidation(  //  ver que ese texarea sera un select
                    {
                        preventSubmit: true,
                        submitError: function($form, event, errors) {
                            // something to have when submit produces an error ?
                            // Not decided if I need it yet
                        },
                        submitSuccess: function($form, event) {
                            event.preventDefault(); // prevent default submit behaviour
                            // get values from FORM
                            $('#cargando').show("slow");  // show the loading message.
                            var dbname = $("input#dbname").val();
                            var username = $("input#username").val();
                            var pass = $("input#pass").val();
                            var dbhost = $("input#dbhost").val();
                            var prefijo = $("input#prefijo").val();


                            $.ajax({
                                url: "configura_BBDD.php",
                                type: "POST",
                                data: {dbname: dbname, username: username, pass: pass, dbhost: dbhost, prefijo: prefijo},
                                cache: false,
                                success: function(data) {
                                    $('#cargando').hide("slow");  // show the loading message.
                                    var result = data.trim().split("#");
                                    if (result[0] == 'OK') {
                                        $("#FormBBDD").hide("slow");
                                        $("#1_fase1").hide("slow");
                                        $("#2_fase1").hide("slow");
                                        $('#success').html(" <p>&nbsp;</p>" + result[1] + " <p>&nbsp;</p>");
                                        $('#success').show();
                                        $('#segunda_fase').show("slow");
                                        $('#FormBBDD').trigger("reset");
                                    } else {
                                        $("#success2").html("<div class=\"alert alert-warning\">Se ha producido un error: " + result[1]+ " </div>");
                                        $("#success2").show("slow");

                                    }
                                },
                                error: function() {
                                    // Fail message
                                    $('#success2').html("<div class='alert alert-danger'>");
                                    $('#success2 > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                                            .append("</button>");
                                    $('#success2 > .alert-danger').append("<strong>Sorry " + firstName + " uppps! el servidor no esta respondiendo...</strong> Intetelo despues. Perdone por las molestias!");
                                    $('#success2 > .alert-danger').append('</div>');
                                },
//incluido para hacer pruebas, cuando funcione quitar y recuperar lo de arriba
                                error:function(xhr, ajaxOptions, thrownError) {
                                    alert(xhr.status);
                                    alert(thrownError);
                                },
                                        //hasta aqui


                            })
                        },
                        filter: function() {
                            return $(this).is(":visible");
                        },
                    });

            $("a[data-toggle=\"tab\"]").click(function(e) {
                e.preventDefault();
                $(this).tab("show");
            });
        });


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success2').html('');
});