-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-12-2015 a las 19:02:06
-- Versión del servidor: 5.5.16
-- Versión de PHP: 5.5.11



-- base de datos para versiones anteriores a mysql5.6 que no permiten indices fulltext

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `votaciones_demokratian_2.3.x`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_candidatos`
--

DROP TABLE IF EXISTS `dk_candidatos`;
CREATE TABLE IF NOT EXISTS `dk_candidatos` (
  `ID` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_provincia` smallint(3) unsigned zerofill NOT NULL,
  `texto` text COLLATE utf8_spanish_ci NOT NULL,
  `nombre_usuario` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `sexo` char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'H',
  `id_votacion` int(6) unsigned zerofill NOT NULL,
  `anadido` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_anadido` datetime NOT NULL,
  `modif` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_modif` datetime NOT NULL,
  `id_vut` int(6) NOT NULL DEFAULT '0',
  `imagen` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `imagen_pequena` tinytext COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  KEY `fk_dk_candidatos_dk_votacion1_idx1` (`id_votacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla de los candidatos u opciones de una votación' AUTO_INCREMENT=1 ;

--
-- RELACIONES PARA LA TABLA `dk_candidatos`:
--   `id_votacion`
--       `dk_votacion` -> `ID`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_ccaa`
--

DROP TABLE IF EXISTS `dk_ccaa`;
CREATE TABLE IF NOT EXISTS `dk_ccaa` (
  `ID` int(3) unsigned zerofill NOT NULL,
  `ccaa` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `especial` int(3) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de comunidades autonomas de España';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_codificadores`
--

DROP TABLE IF EXISTS `dk_codificadores`;
CREATE TABLE IF NOT EXISTS `dk_codificadores` (
  `ID` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nombre` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `nivel_acceso` smallint(4) NOT NULL DEFAULT '10',
  `correo` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `nif` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `pass` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `usuario` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_ultima` datetime NOT NULL,
  `codigo_rec` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `clave_publica` text COLLATE utf8_spanish_ci NOT NULL,
  `clave_privada` text COLLATE utf8_spanish_ci NOT NULL,
  `id_votacion` int(6) unsigned zerofill NOT NULL,
  `anadido` int(10) unsigned zerofill NOT NULL,
  `fecha_anadido` datetime NOT NULL,
  `modif` int(19) unsigned zerofill NOT NULL,
  `fecha_modif` datetime NOT NULL,
  `orden` int(2) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_dk_codificadores_dk_votacion1_idx1` (`id_votacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla donde se reflejan los las personas que pueden encriptar y desencripta una votacion' AUTO_INCREMENT=1 ;

--
-- RELACIONES PARA LA TABLA `dk_codificadores`:
--   `id_votacion`
--       `dk_votacion` -> `ID`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_debate_comentario`
--

DROP TABLE IF EXISTS `dk_debate_comentario`;
CREATE TABLE IF NOT EXISTS `dk_debate_comentario` (
  `idcomentario` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(10) unsigned zerofill NOT NULL,
  `id_votacion` int(6) unsigned zerofill NOT NULL,
  `tema` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `fecha` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `comentario` text COLLATE utf8_spanish_ci NOT NULL,
  `estado` tinytext COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idcomentario`),
  KEY `fk_dk_debate_comentario_dk_votantes1_idx` (`id_usuario`),
  KEY `fk_dk_debate_comentario_dk_votacion1_idx` (`id_votacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla que almacena los comentarios en el tipo debate' AUTO_INCREMENT=1 ;

--
-- RELACIONES PARA LA TABLA `dk_debate_comentario`:
--   `id_usuario`
--       `dk_votantes` -> `ID`
--   `id_votacion`
--       `dk_votacion` -> `ID`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_debate_preguntas`
--

DROP TABLE IF EXISTS `dk_debate_preguntas`;
CREATE TABLE IF NOT EXISTS `dk_debate_preguntas` (
  `ID` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `pregunta` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `respuestas` char(2) COLLATE utf8_spanish_ci NOT NULL,
  `id_votacion` int(6) unsigned zerofill NOT NULL,
  `anadido` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_anadido` datetime NOT NULL,
  `modif` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_modif` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  KEY `fk_dk_debate_preguntas_dk_votacion1_idx` (`id_votacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla que almacena las preguntas en el tipo de votacion  debate' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_debate_votos`
--

DROP TABLE IF EXISTS `dk_debate_votos`;
CREATE TABLE IF NOT EXISTS `dk_debate_votos` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `id_votante` int(10) unsigned zerofill NOT NULL,
  `id_pregunta` int(6) unsigned zerofill NOT NULL,
  `voto` char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT '0',
  `id_votacion` int(6) unsigned zerofill NOT NULL,
  `codigo_val` char(128) COLLATE utf8_spanish_ci NOT NULL,
  UNIQUE KEY `ID` (`ID`),
  KEY `fk_dk_debate_votos_dk_debate_preguntas1_idx` (`id_pregunta`),
  KEY `fk_dk_debate_votos_dk_votacion1_idx` (`id_votacion`),
  KEY `fk_dk_debate_votos_dk_votantes1_idx` (`id_votante`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla que almacena los votos en el tipo debate' AUTO_INCREMENT=1 ;

--
-- RELACIONES PARA LA TABLA `dk_debate_votos`:
--   `id_pregunta`
--       `dk_debate_preguntas` -> `ID`
--   `id_votacion`
--       `dk_votacion` -> `ID`
--   `id_votante`
--       `dk_votantes` -> `ID`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_elvoto`
--

DROP TABLE IF EXISTS `dk_elvoto`;
CREATE TABLE IF NOT EXISTS `dk_elvoto` (
  `ID` char(64) COLLATE utf8_spanish_ci NOT NULL,
  `id_provincia` int(3) unsigned zerofill NOT NULL,
  `id_candidato` smallint(6) unsigned zerofill NOT NULL,
  `voto` text COLLATE utf8_spanish_ci NOT NULL,
  `id_votacion` int(6) unsigned zerofill NOT NULL,
  `codigo_val` char(128) COLLATE utf8_spanish_ci NOT NULL,
  `especial` smallint(1) NOT NULL DEFAULT '0',
  `incluido` tinytext COLLATE utf8_spanish_ci NOT NULL,
  UNIQUE KEY `ID` (`ID`),
  KEY `id_candidato` (`id_candidato`),
  KEY `fk_dk_elvoto_dk_votacion1_idx` (`id_votacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla que almacena los votos cuando es una votacion tipo VUT';

--
-- RELACIONES PARA LA TABLA `dk_elvoto`:
--   `id_votacion`
--       `dk_votacion` -> `ID`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_grupo_trabajo`
--

DROP TABLE IF EXISTS `dk_grupo_trabajo`;
CREATE TABLE IF NOT EXISTS `dk_grupo_trabajo` (
  `ID` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `tipo` smallint(1) NOT NULL DEFAULT '0',
  `subgrupo` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `especial` int(3) NOT NULL,
  `id_provincia` int(3) NOT NULL,
  `id_ccaa` int(3) NOT NULL,
  `texto` text COLLATE utf8_spanish_ci NOT NULL,
  `acceso` smallint(1) NOT NULL,
  `activo` smallint(1) NOT NULL,
  `creado` smallint(10) unsigned zerofill NOT NULL,
  `tipo_votante` char(2) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla con los grupos de trabajo' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_interventores`
--

DROP TABLE IF EXISTS `dk_interventores`;
CREATE TABLE IF NOT EXISTS `dk_interventores` (
  `ID` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_provincia` smallint(3) unsigned zerofill NOT NULL,
  `nombre` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `correo` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `id_votacion` int(6) unsigned zerofill NOT NULL,
  `anadido` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_anadido` datetime NOT NULL,
  `modif` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_modif` datetime NOT NULL,
  `pass` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `usuario` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `tipo` int(1) NOT NULL DEFAULT '0',
  `codigo_rec` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_ultimo` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  KEY `fk_dk_interventores_dk_votacion1_idx1` (`id_votacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de los interventores que participan en una votación' AUTO_INCREMENT=1 ;

--
-- RELACIONES PARA LA TABLA `dk_interventores`:
--   `id_votacion`
--       `dk_votacion` -> `ID`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_municipios`
--

DROP TABLE IF EXISTS `dk_municipios`;
CREATE TABLE IF NOT EXISTS `dk_municipios` (
  `id_municipio` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `id_provincia` int(3) unsigned zerofill NOT NULL,
  `cod_municipio` int(11) NOT NULL COMMENT 'Código de muncipio DENTRO de la provincia, campo no único',
  `DC` int(11) NOT NULL COMMENT 'Digito Control. El INE no revela cómo se calcula, secreto nuclear.',
  `nombre` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_municipio`),
  KEY `fk_dk_municipios_dk_provincia1_idx` (`id_provincia`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla de Municipios de España' AUTO_INCREMENT=8118 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_nivel_usuario`
--

DROP TABLE IF EXISTS `dk_nivel_usuario`;
CREATE TABLE IF NOT EXISTS `dk_nivel_usuario` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `nivel_usuario` tinytext COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_paginas`
--

DROP TABLE IF EXISTS `dk_paginas`;
CREATE TABLE IF NOT EXISTS `dk_paginas` (
  `id` smallint(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `texto` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `incluido` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `borrable` char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'si',
  `pagina` smallint(2) NOT NULL,
  `zona_pagina` smallint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_provincia`
--

DROP TABLE IF EXISTS `dk_provincia`;
CREATE TABLE IF NOT EXISTS `dk_provincia` (
  `ID` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `provincia` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `especial` int(3) NOT NULL,
  `id_ccaa` int(3) unsigned zerofill NOT NULL,
  `correo_notificaciones` tinytext COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_dk_provincia_dk_ccaa1_idx` (`id_ccaa`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de provincias de España' AUTO_INCREMENT=54 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_usuario_admin_x_provincia`
--

DROP TABLE IF EXISTS `dk_usuario_admin_x_provincia`;
CREATE TABLE IF NOT EXISTS `dk_usuario_admin_x_provincia` (
  `ID` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_usuario` int(10) unsigned zerofill NOT NULL,
  `id_provincia` int(3) unsigned zerofill NOT NULL,
  `admin` int(1) unsigned zerofill NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`,`id_usuario`,`id_provincia`),
  KEY `fk_dk_usuario_admin_x_provincia_dk_votantes1_idx1` (`id_usuario`),
  KEY `fk_dk_usuario_admin_x_provincia_dk_provincia1_idx1` (`id_provincia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla que relaciona los administradores provinciales con las provincias que pueden administrar' AUTO_INCREMENT=1 ;

--
-- RELACIONES PARA LA TABLA `dk_usuario_admin_x_provincia`:
--   `id_usuario`
--       `dk_votantes` -> `ID`
--   `id_provincia`
--       `dk_provincia` -> `ID`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_usuario_x_g_trabajo`
--

DROP TABLE IF EXISTS `dk_usuario_x_g_trabajo`;
CREATE TABLE IF NOT EXISTS `dk_usuario_x_g_trabajo` (
  `ID` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_usuario` int(10) unsigned zerofill NOT NULL,
  `id_grupo_trabajo` int(4) unsigned zerofill NOT NULL,
  `admin` int(1) NOT NULL DEFAULT '0',
  `estado` int(1) NOT NULL DEFAULT '0',
  `razon_bloqueo` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`ID`,`id_usuario`,`id_grupo_trabajo`),
  KEY `fk_dk_usuario_x_g_trabajo_dk_votantes1_idx1` (`id_usuario`),
  KEY `fk_dk_usuario_x_g_trabajo_dk_grupo_trabajo1_idx1` (`id_grupo_trabajo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla que relaciona los grupos de trabajo con los usuarios' AUTO_INCREMENT=1 ;

--
-- RELACIONES PARA LA TABLA `dk_usuario_x_g_trabajo`:
--   `id_usuario`
--       `dk_votantes` -> `ID`
--   `id_grupo_trabajo`
--       `dk_grupo_trabajo` -> `ID`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_usuario_x_votacion`
--

DROP TABLE IF EXISTS `dk_usuario_x_votacion`;
CREATE TABLE IF NOT EXISTS `dk_usuario_x_votacion` (
  `ID` char(64) COLLATE utf8_spanish_ci NOT NULL,
  `id_provincia` int(3) unsigned zerofill NOT NULL,
  `id_votante` int(10) unsigned zerofill NOT NULL,
  `id_votacion` int(6) unsigned zerofill NOT NULL,
  `tipo_votante` char(2) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `correo_usuario` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `ip` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `forma_votacion` tinytext COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`ID`,`id_votante`,`id_votacion`),
  UNIQUE KEY `ID` (`ID`),
  KEY `fk_dk_usuario_x_votacion_dk_votantes1_idx` (`id_votante`),
  KEY `fk_dk_usuario_x_votacion_dk_votacion1_idx` (`id_votacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla que almacena en que votaciones ha votado el Usuario';

--
-- RELACIONES PARA LA TABLA `dk_usuario_x_votacion`:
--   `id_votante`
--       `dk_votantes` -> `ID`
--   `id_votacion`
--       `dk_votacion` -> `ID`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_votacion`
--

DROP TABLE IF EXISTS `dk_votacion`;
CREATE TABLE IF NOT EXISTS `dk_votacion` (
  `ID` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_provincia` int(3) unsigned zerofill NOT NULL,
  `activa` char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'no',
  `nombre_votacion` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `texto` text COLLATE utf8_spanish_ci NOT NULL,
  `resumen` text COLLATE utf8_spanish_ci NOT NULL,
  `tipo` char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  `tipo_votante` char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  `numero_opciones` int(3) NOT NULL,
  `anadido` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_anadido` datetime NOT NULL,
  `modif` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_modif` datetime NOT NULL,
  `fecha_com` datetime NOT NULL,
  `fecha_fin` datetime NOT NULL,
  `activos_resultados` char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'no',
  `fin_resultados` char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'no',
  `id_ccaa` int(2) NOT NULL DEFAULT '0',
  `id_subzona` int(4) NOT NULL DEFAULT '0',
  `id_grupo_trabajo` int(4) unsigned zerofill NOT NULL,
  `demarcacion` int(2) NOT NULL DEFAULT '0',
  `seguridad` int(2) NOT NULL,
  `interventores` int(1) NOT NULL DEFAULT '0',
  `interventor` char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'no',
  `recuento` int(1) NOT NULL DEFAULT '0',
  `id_municipio` smallint(6) NOT NULL DEFAULT '0',
  `encripta` char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'no',
  PRIMARY KEY (`ID`),
  KEY `fk_dk_votacion_dk_provincia1_idx` (`id_provincia`),
  KEY `fk_dk_votacion_dk_grupo_trabajo1_idx` (`id_grupo_trabajo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de las votaciones  existentes' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_votacion_web`
--

CREATE TABLE IF NOT EXISTS `dk_votacion_web` (
  `id` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_votacion` int(6) unsigned zerofill NOT NULL,
  `activo` int(1) DEFAULT NULL,
  `presentacion` int(2) DEFAULT NULL,
  `cabecera` int(2) DEFAULT NULL,
  `imagen_cab` tinytext,
  `aux` tinytext,
  `diseno` text,
  PRIMARY KEY (`id`),
  KEY `fk_dk_votacion_web_dk_votacion1_idx` (`id_votacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla para crear las paginas exteriores de las votaciones' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_votantes`
--

DROP TABLE IF EXISTS `dk_votantes`;
CREATE TABLE IF NOT EXISTS `dk_votantes` (
  `ID` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_provincia` int(3) unsigned zerofill NOT NULL,
  `id_municipio` smallint(6) unsigned NOT NULL,
  `nombre_usuario` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `apellido_usuario` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `nivel_usuario` int(2) NOT NULL DEFAULT '1',
  `nivel_acceso` smallint(4) NOT NULL DEFAULT '10',
  `correo_usuario` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `nif` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `id_ccaa` int(3) unsigned zerofill NOT NULL,
  `pass` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `tipo_votante` char(2) COLLATE utf8_spanish_ci NOT NULL,
  `usuario` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `fecha_ultima` datetime NOT NULL,
  `codigo_rec` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `bloqueo` char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'no',
  `razon_bloqueo` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(200) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'usuario.jpg',
  `imagen_pequena` varchar(200) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'peq_usuario.jpg',
  `perfil` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha_control` date NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_dk_votantes_dk_ccaa1_idx` (`id_ccaa`),
  KEY `fk_dk_votantes_dk_municipios1_idx1` (`id_municipio`),
  KEY `fk_dk_votantes_dk_provincia1_idx1` (`id_provincia`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla donde se reflejan los votantes registrados' AUTO_INCREMENT=2 ;

--
-- RELACIONES PARA LA TABLA `dk_votantes`:
--   `id_ccaa`
--       `dk_ccaa` -> `ID`
--   `id_municipio`
--       `dk_municipios` -> `id_municipio`
--   `id_provincia`
--       `dk_provincia` -> `ID`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_votantes_x_admin`
--

DROP TABLE IF EXISTS `dk_votantes_x_admin`;
CREATE TABLE IF NOT EXISTS `dk_votantes_x_admin` (
  `id` int(12) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `id_votante` int(10) unsigned zerofill NOT NULL,
  `id_admin` int(10) unsigned zerofill NOT NULL,
  `accion` int(1) unsigned zerofill NOT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_dk_votantes_x_admin_dk_votantes1_idx` (`id_votante`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla que registra quien ha incluido  o modificado votantes' AUTO_INCREMENT=1 ;

--
-- RELACIONES PARA LA TABLA `dk_votantes_x_admin`:
--   `id_votante`
--       `dk_votantes` -> `ID`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_votos`
--

DROP TABLE IF EXISTS `dk_votos`;
CREATE TABLE IF NOT EXISTS `dk_votos` (
  `ID` char(64) COLLATE utf8_spanish_ci NOT NULL,
  `id_provincia` int(3) unsigned zerofill NOT NULL,
  `id_candidato` longtext COLLATE utf8_spanish_ci NOT NULL,
  `voto` decimal(7,5) NOT NULL,
  `id_votacion` int(6) unsigned zerofill NOT NULL,
  `codigo_val` char(128) COLLATE utf8_spanish_ci NOT NULL,
  `especial` smallint(1) NOT NULL DEFAULT '0',
  `incluido` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `vote_id` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `position` int(3) NOT NULL,
  `otros` int(3) NOT NULL,
  UNIQUE KEY `ID` (`ID`),
  KEY `fk_dk_votos_dk_votacion1_idx` (`id_votacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla que almacena los votos. Una linea por voto, todos los datos sin encriptar';

--
-- RELACIONES PARA LA TABLA `dk_votos`:
--   `id_votacion`
--       `dk_votacion` -> `ID`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_votos_seg`
--

DROP TABLE IF EXISTS `dk_votos_seg`;
CREATE TABLE IF NOT EXISTS `dk_votos_seg` (
  `ID` char(140) COLLATE utf8_spanish_ci NOT NULL,
  `voto` longtext COLLATE utf8_spanish_ci NOT NULL,
  `cadena` longtext COLLATE utf8_spanish_ci NOT NULL,
  `id_votacion` varchar(40) COLLATE utf8_spanish_ci DEFAULT NULL,
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla que almacena los votos encriptados, el id de la votacion tambien esta encriptado por lo que no tiene relacion directa con el id principal de la tabla de votaciones ';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dk_voto_temporal`
--

DROP TABLE IF EXISTS `dk_voto_temporal`;
CREATE TABLE IF NOT EXISTS `dk_voto_temporal` (
  `ID` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `datos` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `id_votacion` varchar(40) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla que almacena los 5 ultimos votos para luego poder meterlo en la tabla votos seguridad. \nEl id de la votacion tambien esta encriptado por lo que no tiene relacion directa con el id principal de la tabla de votaciones ' AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `dk_candidatos`
--
ALTER TABLE `dk_candidatos`
  ADD CONSTRAINT `fk_dk_candidatos_dk_votacion1` FOREIGN KEY (`id_votacion`) REFERENCES `dk_votacion` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_codificadores`
--
ALTER TABLE `dk_codificadores`
  ADD CONSTRAINT `fk_dk_codificadores_dk_votacion1` FOREIGN KEY (`id_votacion`) REFERENCES `dk_votacion` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_debate_comentario`
--
ALTER TABLE `dk_debate_comentario`
  ADD CONSTRAINT `fk_dk_debate_comentario_dk_votantes1` FOREIGN KEY (`id_usuario`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_debate_comentario_dk_votacion1` FOREIGN KEY (`id_votacion`) REFERENCES `dk_votacion` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_debate_votos`
--
ALTER TABLE `dk_debate_votos`
  ADD CONSTRAINT `fk_dk_debate_votos_dk_debate_preguntas1` FOREIGN KEY (`id_pregunta`) REFERENCES `dk_debate_preguntas` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_debate_votos_dk_votacion1` FOREIGN KEY (`id_votacion`) REFERENCES `dk_votacion` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_debate_votos_dk_votantes1` FOREIGN KEY (`id_votante`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_elvoto`
--
ALTER TABLE `dk_elvoto`
  ADD CONSTRAINT `fk_dk_elvoto_dk_votacion1` FOREIGN KEY (`id_votacion`) REFERENCES `dk_votacion` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_interventores`
--
ALTER TABLE `dk_interventores`
  ADD CONSTRAINT `fk_dk_interventores_dk_votacion1` FOREIGN KEY (`id_votacion`) REFERENCES `dk_votacion` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_usuario_admin_x_provincia`
--
ALTER TABLE `dk_usuario_admin_x_provincia`
  ADD CONSTRAINT `fk_dk_usuario_admin_x_provincia_dk_votantes1` FOREIGN KEY (`id_usuario`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_usuario_admin_x_provincia_dk_provincia1` FOREIGN KEY (`id_provincia`) REFERENCES `dk_provincia` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_usuario_x_g_trabajo`
--
ALTER TABLE `dk_usuario_x_g_trabajo`
  ADD CONSTRAINT `fk_dk_usuario_x_g_trabajo_dk_votantes1` FOREIGN KEY (`id_usuario`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_usuario_x_g_trabajo_dk_grupo_trabajo1` FOREIGN KEY (`id_grupo_trabajo`) REFERENCES `dk_grupo_trabajo` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_usuario_x_votacion`
--
ALTER TABLE `dk_usuario_x_votacion`
  ADD CONSTRAINT `fk_dk_usuario_x_votacion_dk_votantes1` FOREIGN KEY (`id_votante`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_usuario_x_votacion_dk_votacion1` FOREIGN KEY (`id_votacion`) REFERENCES `dk_votacion` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_votacion_web`
--
ALTER TABLE `dk_votacion_web`
  ADD CONSTRAINT `fk_dk_votacion_web_dk_votacion1` FOREIGN KEY (`id_votacion`) REFERENCES `dk_votacion` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_votantes`
--
ALTER TABLE `dk_votantes`
  ADD CONSTRAINT `fk_dk_votantes_dk_ccaa1` FOREIGN KEY (`id_ccaa`) REFERENCES `dk_ccaa` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_votantes_dk_municipios1` FOREIGN KEY (`id_municipio`) REFERENCES `dk_municipios` (`id_municipio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_dk_votantes_dk_provincia1` FOREIGN KEY (`id_provincia`) REFERENCES `dk_provincia` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_votantes_x_admin`
--
ALTER TABLE `dk_votantes_x_admin`
  ADD CONSTRAINT `fk_dk_votantes_x_admin_dk_votantes1` FOREIGN KEY (`id_votante`) REFERENCES `dk_votantes` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `dk_votos`
--
ALTER TABLE `dk_votos`
  ADD CONSTRAINT `fk_dk_votos_dk_votacion1` FOREIGN KEY (`id_votacion`) REFERENCES `dk_votacion` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
