<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="es"><head>
        <meta charset="utf-8">
        <title>Sistema de instalación de DEMOKRATIAN</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content=" ">
        <meta name="author" content=" ">
        <link rel="icon"  type="image/png"  href="../temas/demokratian/imagenes/icono.png"> 




        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/demokratian/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!--    <link href="temas/emokratian/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">-->
        <link href="../temas/demokratian/estilo.css" rel="stylesheet">
        <!--<link href="temas/emokratian/estilo_login.css" rel="stylesheet">-->
    </head>

    <body>

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="cabecera_instalacion.png" class="img-responsive" alt="Logo DEMOKRATIAN">

            </div>

            <!-- END cabecera
            ================================================== -->
            <div id="success"> </div> <!-- mensajes -->

            <div class="row" id="1_fase1">    
                <div class="col-lg-6">
                    <div class="well">
                        <h2>Instalador del sistema de votaciones DEMOKRATIAN</h2>


                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="well">
                        <p> <div class="alert alert-danger">Recuerde borrar la carpeta <strong>"install" </strong>de su servidor una vez que haya terminado la instalación para evitar problemas.</div></p>             

                    </div>
                </div>
            </div>



            <div class="row" id="2_fase1">  
                <div class="col-lg-12">   
                    <div class="well">
                        <form action="prueba.php" method="post"  class="form-horizontal" role="form" name="FormBBDD" id="FormBBDD" >
                            <h3 class="form-signin-heading">Segundo paso</h3>

                            <div class="alert alert-info"> Tranquil@ , que estos datos podra modificarlos si es necesario posteriormente mediante el sistema de administracion de DEMOKRATIAN </div>

                            <div id="success2"> </div>

                            <fieldset>
                                <legend>Otros datos de configuración</legend>
                                
                                 <div class="form-group">
                                    <label for="dbhost" class="col-sm-4 control-label">Numero de circunscripciones</label>
                                    <div class="col-sm-4">

                                        <label>
                                            <input name="tipo_circuns" type="radio" id="tipo_circuns_0" value="false" checked="CHECKED">
                                            por defecto 4 false </label>

                                      <label>
                                            <input name="tipo_circuns" type="radio" id="tipo_circuns_1" value="true">
                                            true Solo una</label>
                                        <br>
 
                                    </div>
                                    <div class="col-sm-4">
                                      <p>Por defecto crea circunscripciones estatal, autonómica, provincial  y municipal</p>
                                    </div>
                                </div>
                                
                                
                                 <div class="form-group">
                                    <label for="Correo_insti" class="col-sm-4 control-label">Usar correos institucionales</label>
                                    <div class="col-sm-4">

                                     
                                        <label>
                                          <input name="correo_insti" type="radio" id="correo_insti_0" value="false" onClick="deshabilita_local()" checked="CHECKED">
                                          NO false
                                        </label>
                                      <label>
                                          <input name="correo_insti" type="radio" id="correo_insti_1" value="true" onClick="habilita_local()">
                                          SI true
                                        </label>

                                    (Si usa esta opción debe marcar circunscripcion unica)</div>
                                    <div class="col-sm-4">
                                      <p>Permite votar usando correos institucionales de una empresa, ong, universidad, etc sin necesidad de registro previo por el administrador (solo podran votar los que tengan un correo de ese tipo). </p>
                                    </div>
                                </div>
                                
                             <div id="div_local"  class="caja_de_display" style="display:none" >
                                <div class="form-group">
                                   	    <label for="elCorreo_insti" class="col-sm-4 control-label"> </label>
                                    <div class="col-sm-4"> 
                                        <input type="text" name="elCorreo_insti" id="elCorreo_insti" class="form-control" size="25"  placeholder="@midominio.es"  autofocus>
                                    </div>
                                    <div class="col-sm-4">
                                      <p>dominio del correo incluyendo la @</p>
                                    </div>
                                </div>                                     
                                     
                                <div class="form-group">
                                        <label for="nombre_empresa" class="col-sm-4 control-label"> </label>
                                    <div class="col-sm-4">
                                        <input type="text" name="nombre_empresa" id="nombre_empresa" class="form-control" size="25"  placeholder="Nombre de la institucion, ong,empresa..."  autofocus>                                        
                                    </div>
                                    <div class="col-sm-4">
                                      <p>Nombre de la ONG, Institición, universidad, empresa...</p>
                                  </div>                                       
                                </div>
                             </div><br/>
                                                                      
                                <div class="form-group">
                                    <label for="nombre_web" class="col-sm-4 control-label">Nombre de la web</label>
                                    <div class="col-sm-4">
                                        <input name="nombre_web" id="nombre_web" type="text" class="form-control" size="25"  placeholder="DEMOKRATIAN |  Centro de votaciones" required  autofocus /></td>
                                    </div>
                                    <div class="col-sm-4">

                                    </div>
                                </div>               


                                <div class="form-group">
                                    <label for="theme" class="col-sm-4 control-label">Tema del sitio</label> 
                                    <div class="col-sm-4">
                                        <?php
                                        $carpeta = "../temas"; //ruta actual
                                        if (is_dir($carpeta)) {
                                            if ($dir = opendir($carpeta)) {
                                                while (($archivo = readdir($dir)) !== false) {
                                                    if ($archivo != '.' && $archivo != '..' && $archivo != '.htaccess' && $archivo != 'index.html' && $archivo != 'index.php') {

                                                        if ($archivo == "demokratian") {
                                                            $check = "selected=\"selected\" ";
                                                        } else {
                                                            $check = "";
                                                        }
                                                        $lista .="<option value=\"" . $archivo . "\" $check > " . $archivo . "</option>";
                                                        $lista.=$archivo;
                                                    }
                                                }
                                                closedir($dir);
                                            }
                                        }
                                        ?>
                                        <select name="theme" class="form-control"  id="theme" >
                                            <?php echo "$lista"; ?>
                                        </select>

                                    </div>
                                    <div class="col-sm-4">
                                        Si no ha añadido uno deje el tema por defecto "demokratian". 
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <legend>Configuración del servidor de correo</legend>
								<div class="form-group">
                                    <label for="dbhost" class="col-sm-4 control-label">Forma de envio</label>
                                    <div class="col-sm-4">

                                        <label>
                                            <input name="smtp" type="radio" id="smtp_0" value="true" checked="CHECKED" onClick="habilita_smtp()">
                                            smtp TRUE</label>
										<label>
                                            <input name="smtp" type="radio" id="smtp_2" value="false"  onClick="deshabilita_smtp()">
                                            smtp FALSE</label>
                                        <label>
                                            
                                        <br>

                                    </div>
                                    <div class="col-sm-4">
                                     Si usamos envíos smtp poner true. <br/>Si queremos que el envío se realice con la función phpmail() usar false (desaconsejado) . Muy recomendado usar smtp <strong>true.</strong>.
                                    </div>
                                    </div>
                                 <div id="div_smtp"  class="caja_de_display" >
                                    
                                <div class="form-group">
                                    <label for="theme" class="col-sm-4 control-label">servidor de correo</label> 
                                    <div class="col-sm-4">
                                        <input name="server_correo" type="text"  class="form-control" id="server_correo"  placeholder="smtp.MiServidor.net" size="25"  /> 
                                    </div>
                                    <div class="col-sm-4">

                                    </div>
                                    </div>

                                <div class="form-group">
                                    <label for="user_correo" class="col-sm-4 control-label">Usuario  de correo</label> 
                                    <div class="col-sm-4">
                                        <input name="user_correo" type="text"  class="form-control" id="user_correo"  placeholder="nombre usuario del correo" size="25" /> 
                                    </div>
                                    <div class="col-sm-4">

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="pass_correo" class="col-sm-4 control-label">Contraseña</label>
                                    <div class="col-sm-4">
                                        <input name="pass_correo" id="pass_correo" type="text" class="form-control" size="25" placeholder="contraseña del correo"  />
                                    </div>
                                    <div class="col-sm-4"> Contraseña de correo.
                                    </div>
                                </div>        

                                <div class="form-group">
                                    <label for="dbhost" class="col-sm-4 control-label">Puerto del servidor de correo</label>
                                    <div class="col-sm-4">
                                        <input name="puerto_correo" id="puerto_correo" class="form-control" type="text" size="25" value="25" required  autofocus />
                                    </div>
                                    <div class="col-sm-4">
                                        Por defecto <strong>25</strong>.
                                    </div>
                                </div>        
         
                                <div class="form-group">
                                    <label for="dbhost" class="col-sm-4 control-label">SMTPSecure</label>
                                    <div class="col-sm-4">

                                        <label>
                                            <input name="SMTPSecure" type="radio" id="SMTPSecure_0" value="false" checked="CHECKED">
                                            Deshabilitado</label>
										<label>
                                            <input name="SMTPSecure" type="radio" id="SMTPSecure_2" value="TLS" >
                                            TLS</label>
                                        <label>
                                            <input name="SMTPSecure" type="radio"  id="SMTPSecure_1" value="SSL" >
                                            SSL</label>
                                        <br>

                                    </div>
                                    <div class="col-sm-4">
                                        Por defecto <strong>Deshabilitado</strong>.
                                    </div>
                                    </div>
                                    
                                <div class="form-group">
                                    <label for="dbhost" class="col-sm-4 control-label">Autentificación por SMTP (SMTPAuth)</label>
                                    <div class="col-sm-4">

                                        <label>
                                            <input name="SMTPAuth" type="radio" id="SMTPAuth_0" value="false">
                                            FALSE</label>

                                        <label>
                                            <input name="SMTPAuth" type="radio" id="SMTPAuth_1" value="true" checked="CHECKED">
                                            TRUE</label>
                                        <br>

                                    </div>
                                    <div class="col-sm-4">
                                         Por defecto <strong>True</strong>.
                                    </div>
                                    </div>
                                    
                                <div class="form-group">
                                    <label for="dbhost" class="col-sm-4 control-label">Forma de envio de correo</label>
                                    <div class="col-sm-4">

                                        <label>
                                            <input name="tipo_envio" type="radio" id="tipo_envio_0" value="false" checked="CHECKED">
                                            IsSMTP</label>

                                        <label>
                                            <input type="radio" name="tipo_envio" value="true" id="tipo_envio_1">
                                            IsSendMail</label>
                                        <br>

                                    </div>
                                    
                                    <div class="col-sm-4">
                                        Algunos sevidores como 1&1 hay que usar IsSendMail. Por defecto <strong>IsSMTP</strong>.
                                    </div>
                                </div>
                                
                                                                <div class="form-group">
                                    <label for="dbhost" class="col-sm-4 control-label">Envio html o texto plano(IsHTML)</label>
                                    <div class="col-sm-4">

                                        <label>
                                            <input name="IsHTML" type="radio" id="IsHTML_0" value="false" checked="CHECKED">
                                            FALSE</label>

                                        <label>
                                            <input name="IsHTML" type="radio" id="IsHTML_1" value="true">
                                            TRUE</label>
                                        <br>

                                    </div>
                                    <div class="col-sm-4">
                                         Por defecto <strong>False</strong>.
                                    </div>
                                    </div>
                                    </div>
                                </fieldset>
                            <fieldset>
                                <legend>Direccion de correo principal del sistema de votaciones</legend>

                                <div class="form-group">
                                    <label for="email_env" class="col-sm-4 control-label">Direccion de correo general</label>
                                    <div class="col-sm-4">
                                        <input name="email_env" id="email_env" class="form-control" type="text"   size="25" required  placeholder="info1@demokratian.org" autofocus />
                                    </div>
                                    <div class="col-sm-4">       
                                      Posteriormente mediante el panel de administración podra cambiar y configurar otras direcciones de correo.
                                    </div>
                                </div>       



                            </fieldset>	

                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-6">
                                    <button class="btn btn-ttc btn-lg btn-primary btn-block" type="submit">Configurar</button>
                                </div>
                            </div>
                        </form>



                    </div>

                </div> 

            </div>
            <div id="cargando" > <img class="cargador" src='../temas/demokratian/imagenes/loading.gif'/> <div  class="cargador2" > <h4>Esto puede tardar unos minutos...</h4></div>
            </div>
            <!--segunda fase de la configuración-->
            <div class="row" id="tercera_fase"> <div class="col-lg-12"> <br/>
                    <p class="bg-success"> Ha terminado con la segunda fase de la configuración, enseguida terminamos, puede pasar a la tercera fase &nbsp;&nbsp;<a href="install3.php" class="btn btn-primary btn-lg active" role="button">Tercera fase</a></p>
                    <br/></div>
            </div>
            <!--fin segunda fase-->

            <div id="footer" class="row">
               <div  class="pie_demokratia"> 
                
                    <div class="pie_demokratia2"> 
                        <a href="http://www.demokratian.org" target="_blank"><img src="../temas/demokratian/imagenes/logo_pie.png" class="img-responsive"  alt="DEMOKRATIA | plataforma de votaciones"  /></a>
                    </div>
                    <div class="pie_demokratia1"> </div>
                
                </div>
            </div>




        </div>  

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#tercera_fase").hide();
                $("#cargando").hide();
            });

            $("#cargando").on("ajaxStart", function() {
                // this hace referencia a la div con la imagen. 
                $(this).show();
            }).on("ajaxStop", function() {
                $(this).hide();
            });
        </script>
   <!--<script src="js/jquery.validate.js"></script>-->
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script src="../js/jqBootstrapValidation.js"></script>
        <script src="configura_2.js"></script>
        <script type='text/javascript'>
			function habilita_smtp() {
				document.getElementById('div_smtp').style.display = 'block';			
			 }
            function deshabilita_smtp() {
				document.getElementById('div_smtp').style.display = 'none';			
			 }
        
        </script>
          <script type='text/javascript'>
			function habilita_local() {
				document.getElementById('div_local').style.display = 'block';			
			 }
            function deshabilita_local() {
				document.getElementById('div_local').style.display = 'none';			
			 }
        
        </script>
    </body>
</html>