<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
require_once("../config/config.inc.php");
require_once("../inc_web/conexion.php");
include('seguri_inter.php');
?>


<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><?php echo "$nombre_web"; ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content=" ">
        <meta name="author" content=" ">
        
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png"> 



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="../modulos/DataTables-1.10.3/media/css/jquery.dataTables.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->


            <div class="row">




                <div class="col-md-2" >             
                    <?php include("menu.php"); ?>
                </div>



                <div class="col-md-10">

                    <!--Comiezo-->
                    <h2>Votos incluidos manualmente en la votacion</h2><h2> <strong><?php echo $nombre_votacion; ?></strong></h2>       
                    <?php
                    // Votos en urna
                    $sql = "select distinct vote_id from $tbn10 WHERE id_votacion = '$idvot' and especial=1";
                    $result = mysqli_query($con, $sql);
                    $urna = mysqli_num_rows($result); // obtenemos el número de filas
                    ?>

                    <div class="jumbotron">
                        <?php if ($urna != 0) { ?>
                            <p class="lead">Votos introducidos de urna: <?php echo "$urna" ?></p>
                        <?php } ?>
                    </div>  
                    
                    
                    
            <h2>Candidatos u opciones de esta votación</h2>  

            <table class="table table-striped">
                <tr>

                    <th width="10%">Identificador</th>
                    <th width="80%">Nombre</th>
                    <th width="10%">Sexo</th>
                </tr>   
                <?php
// sacamos los datos del array

                $sql2 = "SELECT ID, nombre_usuario,sexo  FROM $tbn7 WHERE id_votacion=" . $idvot . " ";
                $result2 = mysqli_query($con, $sql2);
                if ($row2 = mysqli_fetch_array($result2)) {
                    mysqli_field_seek($result2, 0);

                    do {
                        ?>	 
                        <tr>
                            <td><?php echo $row2[0]; ?></td>
                            <td><?php echo $row2[1]; ?></td>
                            <td><?php echo $row2[2]; ?></td>


                        </tr>

                        <?php
                    } while ($row2 = mysqli_fetch_array($result2));
                }
                ?>

            </table>      



            <h2>Lista de todos los votos incluidos manualmente en esta votacion</h2>    
            <p> El segundo dato corresponde al identificador del candidato u opcion y la puntuacion asignada dependiendo del orden que se haya marcado </p>
            <p> Los distintos candidatos u opciones estan separados por comas</p>                
                    <?php
                    //$sql = "SELECT a.id_candidato, a.incluido ,b.nombre_usuario  FROM $tbn10 a, $tbn7 b WHERE (a.id_candidato=b.ID) and a.id_votacion = '$idvot' and a.especial =1";
					$sql = "SELECT id_candidato, incluido   FROM $tbn10 WHERE  id_votacion = '$idvot' and especial =1";

                    $result = mysqli_query($con, $sql);

                    if ($row = mysqli_fetch_array($result)) {
                        $i = 1;
                        ?>

                        <table id="tabla1" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="5%">&nbsp;</th>
                                    <th width="70%">Voto</th>

                                    <th width="25%">Interventores</th>



                                </tr>
                            </thead>

                            <tbody>

                                <?php
                                mysqli_field_seek($result, 0);

                                do {
                                    ?> 


                                    <tr <?php
                                    if ($row[0] == 1) {
                                        echo "class=\"linea\" ";
                                    }
                                    ?> >
                                        <td><?php echo $i++ ?> </td>
                                        <td> <?php echo "$row[0]" ?> </td>
                                        <td>  <?php echo "$row[1]" ?> </td>
                                    </tr>



                                    <?php
                                } while ($row = mysqli_fetch_array($result));
                                ?>
                            </tbody>
                        </table>

                        <?php
                    } else {
                        
                    }
                    ?>

				<p></p>
                    <!--Final-->
                </div>



            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->

                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>  

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->    
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script src="../modulos/DataTables-1.10.3/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" class="init">

            $(document).ready(function() {
                $('#tabla1').dataTable({
                    "language": {
                        "lengthMenu": "Ver _MENU_  resultados por pagina",
                        "zeroRecords": "No se han encontrado resultados - perdone",
                        "info": "Mostrando _PAGE_ de _PAGES_ paginas",
                        "infoEmpty": "No se han encitrado resultados",
                        "infoFiltered": "(filtered from _MAX_ total records)",
                        "loadingRecords": "Cargando...",
                        "processing": "Procesando...",
                        "search": "Buscar:",
                        "paginate": {
                            "first": "Primero",
                            "last": "Ultimo",
                            "next": "Siguiente",
                            "previous": "Anterior"
                        },
                        "aria": {
                            "sortAscending": ": activate to sort column ascending",
                            "sortDescending": ": activate to sort column descending"
                        }
                    }

			
                });
            });
        </script>
    </body>
</html>