<?php

###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los t�rminos de la Licencia P�blica General de GNU seg�n es publicada por la ###
### Free Software Foundation, bien de la versi�n 3 de dicha Licencia o bien de cualquier versi�n posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea �til, pero SIN NINGUNA GARANT�A, incluso sin la garant�a MERCANTIL impl�cita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROP�SITO PARTICULAR. V�ase la Licencia P�blica General de GNU para m�s detalles.                                               ###
### Deber�a haber recibido una copia de la Licencia P�blica General junto con este programa. Si no ha sido as�, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### Tambi�n puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
require_once("../config/config.inc.php");
require_once("../inc_web/conexion.php");
//include('seguri_inter.php');
require ("../basicos_php/basico.php");


$process_result = 'OK';
$msg_result = '';

$idvot = fn_filtro_numerico($con, $_POST['id_vot']);
$id_provincia = fn_filtro_numerico($con, $_POST['id_provincia']);
$valores = fn_filtro($con, $_POST['valores']);
$id_ccaa = fn_filtro($con, $_POST['id_ccaa']); 
$id_subzona = fn_filtro($con, $_POST['id_subzona']);
$id_grupo_trabajo = fn_filtro($con, $_POST['id_grupo_trabajo']);
$demarcacion = fn_filtro($con, $_POST['demarcacion']);
$clave_seg = fn_filtro($con, $_POST['clave_seg']);
$recuento = fn_filtro($con, $_POST['recuento']);
$mixto = fn_filtro($con, $_POST['mixto']);
$encripta = fn_filtro($con, $_POST['encripta']);
$inter = fn_filtro($con, $_POST['inter']);
if (isset($_POST['blanco'])) {
    $blanco = true;
} else {
    $blanco = false;
}
if (isset($_POST['nulo'])) {
    $nulo = true;
} else {
    $nulo = false;
}


$sql3 = "SELECT seguridad, nombre_votacion, numero_opciones,id_municipio,id_grupo_trabajo FROM $tbn1 WHERE ID='$idvot' ";
$resulta3 = mysqli_query($con, $sql3) or die("error: " . mysqli_error());

while ($listrows3 = mysqli_fetch_array($resulta3)) {
    $seguridad = $listrows3[seguridad];
    $nombre_votacion = $listrows3[nombre_votacion];
    $numero_opciones = $listrows3[numero_opciones];
    $id_municipio = $listrows3[id_municipio];
    $id_grupo_trabajo = $listrows3[id_grupo_trabajo];
}

if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} elseif (isset($_SERVER['HTTP_VIA'])) {
    $ip = $_SERVER['HTTP_VIA'];
} elseif (isset($_SERVER['REMOTE_ADDR'])) {
    $ip = $_SERVER['REMOTE_ADDR'];
}


$forma_votacion = 1;
$array_valores = explode(";", $valores);

    // CHECK VOTACIO
    $men = array();
    $women = array();
    $total = 0;
    foreach ($array_valores as $v) {
        $array_valor = explode(",", $v);
        $position = $array_valor[0];
        $candi = $array_valor[1];
        if ($position == "") {
            continue;
        }
        $total = $total + 1;

        if ($position < 1 || $position > $numero_opciones) {
            $process_result = "ERROR";
            $msg_result = "Posici&oacute;n erronea en la lista: $v";
            break;
        }
        $sql = "SELECT sexo FROM $tbn7 WHERE id_votacion = '$idvot' and ID = '$candi'";
        $result = mysqli_query($con, $sql);
        if ($row = mysqli_fetch_array($result)) {
            if ($row[0] == "H") {
                if (array_key_exists($position, $men)) {
                    $process_result = "ERROR";
                    $msg_result = "Posici&oacute;n repetida en la lista de hombres: $position";
                    break;
                }
                $men[$position] = $candi;
            } else {
                if (array_key_exists($position, $women)) {
                    $process_result = "ERROR";
                    $msg_result = "Posici&oacute;n repetida en la lista de mujeres: $position";
                    break;
                }
                $women[$position] = $candi;
            }
        } else {
            // candidat no trobat
            $process_result = "ERROR";
            $msg_result = "Candidato no existe en la base de datos ($candi). Notifica el error a la  Comisi&oacute;n de Primarias";
            break;
        }
    }
    // Check blanc
    if ($total == 0) {
        if (!$blanco) {
            $process_result = "ERROR";
            $msg_result = "No se ha seleccionado candidato y no ha indicado que el voto sea en blanco.";
        }
    } else {
        // Check ratio...
        // ugly hack. Check ratio only if numero_opciones > 2
        if ($mixto == "NOesMixto") {
            
        } else {
            if ($numero_opciones > 2) {
                $ratio_men = count($men) / $total;
                if ($ratio_men > 0.6 || $ratio_men < 0.4) {
                    if (!$nulo) {
                        $process_result = "ERROR";
                        $msg_result = "El voto es nulo y no se ha indicado como tal";
                    }
                }
            }
        }
    }


    if ($process_result != "ERROR") {

        $codi = hash("sha512", $clave_seg);
        $okVot = true;

        $text = '';
        for ($i = 0; $i < 6; $i++) {
            $d = rand(1, 30) % 2;
            $text .= $d ? chr(rand(65, 90)) : chr(rand(48, 57));
        }


        $transactionid = "INSVOT" . $text;
        mysqli_query($con, 'BEGIN ' . $transactionid);
        // GET RANDOM ID FOR THE WHOLE VOTE
        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        $cad = "";
        for ($i = 0; $i < 4; $i++) {
            $cad .= substr($str, rand(0, 62), 1);
        }
        $time = microtime(true);
        $timecad = $time . $cad;
        $vote_id = hash("sha256", $timecad);
        $total = 0;
        foreach ($array_valores as $v) {
            $array_valor = explode(",", $v);
            $position = $array_valor[0];
            $candi = fn_filtro($con, $array_valor[1]);
            if ($position == "") {
                continue;
            }
            $total = $total + 1;
            if ($position > 0 && $position <= $numero_opciones) {

                /////introduce el tipo de recuento

                if ($recuento == 0) {
                    $vot = $numero_opciones + 1 - $position; /////introduce valor del voto recuento borda							
                } else if ($recuento == 1) {
                    $vot = 1 / $position; // mete el recuento DOWDALL
                }

                $datos_votado.=$candi . "-" . $vot . ",";
                $datos_del_voto = trim($datos_votado, ','); ///quitamos la ultima coma de la cadena para meterlo en la bbdd
            }
        }

        if ($nulo) {
            $nulo_blanco = 1;
            $datos_votado = "NULO";
            $datos_del_voto = "NULO-1";
        }

        if ($total == 0) {
            // ALTA VOT BLANC
            $nulo_blanco = 2;
            $datos_votado = "BLANCO";
            $datos_del_voto = "BLANCO-1";
        }

        $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        $cad = "";
        for ($i = 0; $i < 4; $i++) {
            $cad .= substr($str, rand(0, 62), 1);
        }
        $time = microtime(true);
        $timecad2 = $cad . $time;
        $res_id = hash("sha256", $timecad2);
 
             $especial = 1; //metemos en la base de datos que ese voto ha sido incluido por interventores

        $provincia=999; // incluimos una provincia inexistente 
        $insql = "insert into $tbn10 (ID,voto,id_candidato,id_provincia,id_votacion,codigo_val,vote_id,position,otros,especial,incluido) values (\"$res_id\",\"$vot\",\"$datos_del_voto\",\"$provincia\",\"$idvot\",\"$codi\",\"$vote_id\",\"$position\",\"$nulo_blanco\",\"$especial\",\"$inter\")";
        $mens = "ERROR en el añadido  voto ";
        $result = db_query($con, $insql, $mens);

        if (!$result) {
            $okVot = false;
            break;
        }

        if (!$okVot) {
            mysqli_query($con, '');
            $process_result = "ERROR";
            $msg_result = "ERROR al introducir su voto en la base de datos.<br/> Intentelo en unos minutos o contacte con el administrador del sitio";
        } else {
                mysqli_query($con, 'COMMIT');
                ////metemos el voto en un txt y lo registramos como medida de seguridad
                $Pollname = md5($idvot);
                $file = $FilePath . $Pollname . "_ballots.txt";
                $fh = fopen($file, 'a');
                fwrite($fh, $datos_votado . PHP_EOL) or die("Could not write file!");
                fclose($fh);  //  Cerramos el fichero 
				
				//añadimo uno en el dato de numero de votos
			    $file2=$FilePath.$Pollname."_tally.txt";
				$fh2 = fopen($file2, 'r+');			
				$búfer = fgets($fh2,100);	
				$partes = explode("|", $búfer);
				$partes1=$partes[1]+1;
				$partes=$partes[0]."|".$partes1;
				fseek($fh2, 0);
				fwrite($fh2, $partes)or die("Could not write file!");				
				fclose($fh2);  //  Cerramos el fichero 

				
                ///// metemos una copia encriptada del voto
                //// primero sacamos los datos de la tabla temporal para hacer una cadena quemeteremos tambien encriptada
                $sql = "SELECT 	ID, datos  FROM $tbn20 where id_votacion=\"$Pollname\" ORDER BY ID DESC";
                $mens = "Error en la consulta a la tabla temporal";
                $result = db_query($con, $sql, $mens);
                if ($row = mysqli_fetch_array($result)) {
                    mysqli_field_seek($result, 0);
                    do {
                        $cadena.=$row[1] . "_";  ////generamos la caena que vamos a meter en la tabla de votos de seguridad
                        $id_borrado = $row[0];
                    } while ($row = mysqli_fetch_array($result));
                }


                $cadena = trim($cadena, '_'); // quitamos de la cadena el ultimo _
                
                $borrado = "DELETE FROM $tbn20 WHERE ID=" . $id_borrado . " ";
                $mens = " error en el borrado de " . $id_borrado;
                $result = db_query($con, $borrado, $mens);


                //metemos los datos codificados en la bbdd
                $ident = md5($timecad2);

                $cadena_temp = $res_id . "+" . $datos_del_voto; //esta variable sirve para esta querry y para la querry de la tabla temporal siguiente
                /////// miramos si hay que encriptar la cadena de voto
                /* if ($encripta=="SHA"){
                  $cadena_temp = hash('sha256', $cadena_temp);
                  }else */
                if ($encripta == "si") {
                    include('../modulos/phpseclib/Crypt/RSA.php');
                    /* require_once "Crypt/AES.php"; */

                    //Function for encrypting with RSA
                    function rsa_encrypt($string, $public_key) {
                        //Create an instance of the RSA cypher and load the key into it
                        $cipher = new Crypt_RSA();
                        $cipher->loadKey($public_key);
                        //Set the encryption mode
                        $cipher->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
                        //Return the encrypted version
                        return base64_encode($cipher->encrypt($string));
                    }

                    // hacemos una busqueda y cargamos la clave publica
                    $result = mysqli_query($con, "SELECT clave_publica FROM $tbn21  WHERE id_votacion='$idvot' ORDER by orden");
                    if ($row = mysqli_fetch_array($result)) {
                        mysqli_field_seek($result, 0);
                        do {
                            $clave_publica = $row[0];

                            $cadena = rsa_encrypt($cadena, $clave_publica);
                        } while ($row = mysqli_fetch_array($result));
                    }
                }

                $shadatovoto = hash('sha256', $cadena_temp);
                $insql1 = "insert into $tbn19 (ID,voto,cadena,id_votacion) values (\"$ident\",\"$shadatovoto\",\"$cadena\",\"$Pollname\")";
                $mens1 = "ERROR en el a�adido voto encriptado ";
                $result = db_query($con, $insql1, $mens1);

                //metemos el dato en la tabla temporal
                $cadena_temp = $res_id . "+" . $datos_del_voto;
                $insql2 = "insert into $tbn20 (datos,fecha,id_votacion) values (\"$cadena_temp\",\"$time\",\"$Pollname\")";
                $mens2 = "ERROR en el añadido  del voto tabla temporal";
                $result = db_query($con, $insql2, $mens2);


                $process_result = 'OK';

                $msg_result = "<div class='alert alert-success'>
			       <p>El voto se ha guardado en la base de datos de forma correcta.<br />Gracias por participar en esta votaci&oacute;n.</p>
			       </div>";
            }
        }
   

mysqli_close($con);
echo $process_result . "#" . $msg_result;
?>
