<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
///// este scrip se usa en varias paginas

if ($var_carga == true) {

    $contar = 0;
///// cogemos los datos de la encuesta

    $result_vot = mysqli_query($con, "SELECT id_provincia, activa,nombre_votacion,tipo_votante, id_grupo_trabajo, demarcacion,id_ccaa, id_municipio  FROM $tbn1 where id=$idvot");
    $row_vot = mysqli_fetch_row($result_vot);

    $id_provincia_vot = $row_vot[0];
    $id_ccaa_vot = $row_vot[6];
    $activa = $row_vot[1];
    $tipo_votante = $row_vot[3];
    $id_grupo_trabajo = $row_vot[4];
    $id_municipio = $row_vot[7];

    if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } elseif (isset($_SERVER['HTTP_VIA'])) {
        $ip = $_SERVER['HTTP_VIA'];
    } elseif (isset($_SERVER['REMOTE_ADDR'])) {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    for ($i = 0; $i < $_SESSION['numero_inter']; $i++) {  //metemos el dato de os interventores que añaden este voto
		$id_inter = "ID_inter_" . $i;
		$inter.=$_SESSION[$id_inter];
		$inter.= "_";
    }

    $inter = trim($inter, '_'); //quitamos el ultimo _
    $ip = $ip . "+" . $nombre . "+" . $inter;  //añadimos la ip y quien ha apuntado la votacion presencial
////si vota presencial metemos el registro

    if ($_GET['votacion'] == "ok") {
        $tipo_voto = "presencial";
        $id = $_GET['id'];

        $usuarios_consulta = mysqli_query($con, "SELECT ID FROM $tbn2 where id_votante='$id' and id_votacion='$idvot'") or die(mysqli_error());

        $total_encontrados = mysqli_num_rows($usuarios_consulta);

        mysqli_free_result($usuarios_consulta);


        if ($total_encontrados != 0) {

            $mensa = "<div class=\"alert alert-warning\">¡¡¡Error!!! <br>El Usuario ya está registrado  o ha votado, operacion incorrecta.</div>";
        } else {



            $result_votante = mysqli_query($con, "SELECT correo_usuario,tipo_votante,id_provincia,nif,nombre_usuario FROM $tbn9 where id=$id");
            $row_votante = mysqli_fetch_row($result_votante);

            $correo_usuario = $row_votante[0];
            $tipo_usuario = $row_votante[1];
            $id_provincia_usu = $row_votante[2];
			$dni = $row_votante[3];
			$nombre_usuario = $row_votante[4];
              // ALTA del usuario en la lista de los que han votado para que no pueda volver a votar
              $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
              $cad = "";
              for ($i = 0; $i < 6; $i++) {
                 $cad .= substr($str, rand(0, 62), 1);
               }
              $time = microtime(true);
              $timecad2 = $time . $cad;
              $user_id = hash("sha256", $timecad2);
              $fecha_env = date("Y-m-d H:i:s");

            $insql_votacion = "insert into $tbn2 (ID, id_provincia, 	id_votante, 	id_votacion, 	tipo_votante, 	fecha, 	correo_usuario, forma_votacion,ip ) values (\"$user_id\",  \"$id_provincia_usu\",  \"$id\", \"$idvot\", \"$tipo_votante\", \"$fecha_env\", \"$correo_usuario\", \"$tipo_voto\",\"$ip\")";
			//$inres = @mysqli_query($con, $insql_votacion) or die("<strong><font color=#FF0000 size=3>  Imposible añadir. Cambie los datos e intentelo de nuevo.</font></strong>");
            $mens = "error añadido de votante de forma presencial ";
            $result = db_query($con, $insql_votacion, $mens);
			 if (!$result) {
                 echo "<div class=\"alert alert-danger\"><strong> UPSSS!!!!<br/>esto es embarazoso, hay un error  </strong> </div>";
              }else{
			$mensa.= "<div class=\"alert alert-success\">Actualizado correctamente $nombre_usuario con nif $dni como votante presencial </div>";  
			  }
        }
    }

//aqui termina el scrip de bloqueo de voto			
    if ($row_vot[5] == 1) {
        $id_provincia_url = $_GET['id_nprov'];
        $sql = "SELECT ID,nombre_usuario , nif,tipo_votante
 FROM $tbn9 a
 WHERE  NOT EXISTS (
	 SELECT * 
	 FROM $tbn2 b
	 WHERE a.id=b.id_votante and b.id_votacion=$idvot 
 ) and a.tipo_votante <=$tipo_votante and a.id_provincia = '$id_provincia_url' ";
    } else if ($row_vot[5] == 2) {
        $sql = "SELECT ID,nombre_usuario , nif,tipo_votante
 FROM $tbn9 a
 WHERE  NOT EXISTS (
	 SELECT * 
	 FROM $tbn2 b
	 WHERE a.id=b.id_votante and b.id_votacion=$idvot 
 ) and a.tipo_votante <=$tipo_votante and a.id_ccaa = '$id_ccaa_vot' ";
    } else if ($row_vot[5] == 3) {
        $sql = "SELECT ID,nombre_usuario , nif,tipo_votante
 FROM $tbn9 a
 WHERE  NOT EXISTS (
	 SELECT * 
	 FROM $tbn2 b
	 WHERE a.id=b.id_votante and b.id_votacion=$idvot 
 ) and a.tipo_votante <=$tipo_votante and a.id_provincia = '$id_provincia_vot' ";
    } else if ($row_vot[5] == 7) {
        $sql = "SELECT ID,nombre_usuario , nif,tipo_votante
 FROM $tbn9 a
 WHERE  NOT EXISTS (
	 SELECT * 
	 FROM $tbn2 b
	 WHERE a.id=b.id_votante and b.id_votacion=$idvot 
 ) and a.tipo_votante <=$tipo_votante and a.id_municipio = '$id_municipio' ";
    } else {
        ///falta los grupos    $sql = "SELECT a.ID, a.nombre_usuario , a.correo_usuario,a.tipo_votante FROM $tbn9 a,$tbn6 b  WHERE (a.ID= b.id_usuario) and id_grupo_trabajo='$id_grupo_trabajo' and a.tipo_votante <='$tipo_votante' ";	

        $sql = "SELECT a.ID, a.nombre_usuario , a.nif,a.tipo_votante
 FROM $tbn9 a,$tbn6 c
 WHERE  NOT EXISTS (
	 SELECT * 
	 FROM $tbn2 b
	 WHERE a.id=b.id_votante  and b.id_votacion=$idvot 
 ) and (a.ID=c.id_usuario) and a.tipo_votante <=$tipo_votante and c.id_grupo_trabajo='$id_grupo_trabajo' ";
    }





    $result = mysqli_query($con, $sql);
    ?>

    <h1>Votacion de <?php echo "$row_vot[2]" ?> </h1>
    <p><?php echo "$mensa"; ?>&nbsp;</p>
    <h3>Listado del censo que NO ha votado <?php if ($row_vot[5] == 1 and $_GET['id_nprov'] != "") { ?>
            para la provincia <strong> <?php echo $_GET['id_nprov']; ?></strong> y tipo de votacion 
        <?php } else if ($row_vot[5] == 2) { ?>
            para la comunidad autonoma <strong><?php echo $row_vot[6]; ?></strong> y tipo de votacion 
        <?php } else if ($row_vot[5] == 3) { ?>
            para la provincia <strong><?php echo $row_vot[0]; ?></strong> y tipo de votacion 
        <?php } else { ?>
            . Tipo de votacion  <?php } ?>

        <?php
        if ($row_vot[3] == 1) {
            echo"solo para socios";
        } else if ($row_vot[3] == 2) {
            echo"solo pata socios y simpatizantes";
        } else if ($row_vot[3] == 3) {
            echo " abierta";
        }
        ?> </h3> 



    <?php
    if ($row = mysqli_fetch_array($result)) {
        ?>
        <form name="form1" method="post" action="<?php $_SERVER['PHP_SELF'] ?>"> 


            <table id="tabla1<?php echo $_GET['cen']; ?>"  cellspacing="0" >
                <thead>
                    <tr>
                        <th width="40%">NOMBRE</th>
                        <th width="30%">NIF</th>
                        <th width="5%">TIPO</th>
                        <th width="10%">VOTA PRESENCIAL</th>
                        

                    </tr>
                </thead>

                <tbody>
                    <?php
                    mysqli_field_seek($result, 0);
                    do {
                        ?>
                        <tr>
                            <td><?php echo "$row[1]" ?></td>
                            <td><?php echo "$row[2]" ?></td>
                            <td><?php
                                if ($row[3] == 1) {
                                    echo"socios";
                                } else if ($row[3] == 2) {
                                    echo"simpatizante verificado";
                                } else if ($row[3] == 3) {
                                    echo"simpatizante";
                                } else if ($row[3] == 5) {
                                    echo "Aqui hay un error";
                                }
                                ?></td>
                            <td>
                                <a href="votantes_listado_multi.php?idvot=<?php echo "$idvot" ?>&votacion=ok&id=<?php echo "$row[0]" ?>&id_nprov=<?php echo $_GET['id_nprov']; ?>&cen=<?php echo $_GET['cen']; ?>&lit=<?php echo $_GET['lit']; ?>"  class=delete>Vota de forma presencial </a>    </td>

                        </tr>
                        <?php
                    } while ($row = mysqli_fetch_array($result));
                    ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>

                </td>
                </tbody>
            </table>

        </form>

        <?php
    } else {
        if ($id_provincia_url == "") {
            echo "<div class=\"alert alert-success\"> Escoja la provincia para la que quiere ver el censo</div>";
        } else {
            echo "<div class=\"alert alert-success\"> ¡No se ha encontrado votantes pendientes de votar!</div> ";
        }
    }
} else {
    echo "error acceso";
}
?>


