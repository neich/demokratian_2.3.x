<?php
require_once("../config/config.inc.php");
require ("../inc_web/conexion.php");
include ('../basicos_php/basico.php');
?>
<!DOCTYPE html>
<html lang="es"><head>
        <meta charset="utf-8">
        <title><?php echo "$nombre_web"; ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content=" ">
        <meta name="author" content=" ">
        <link rel="icon"  type="image/png"  href="temas/<?php echo "$tema_web"; ?>/imagenes/icono.png"> 




        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo_login.css" rel="stylesheet">
    </head>
    <body>

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">

            </div>

            <!-- END cabecera
            ================================================== -->



            <div class="center-block">
                <h1>Pagina de validación de usuario del sistema de votaciones <?php echo "$nombre_web"; ?></h1>
            </div>


            <div class="container"> 

                <fieldset>

                    <form method="post" name="sentMessage"  class="form-signin" id="contactForm"  role="form" >                
                        <!-- <form name="sentMessage" class="well" id="contactForm"  novalidate>-->
                        <!--<legend>Contact me</legend>--> 
                        <div id="caja_contrasena">
                            <?php
                            if ($_GET['regedi'] != "") {

                                function decrypt($string, $key) {
                                    $result = '';
                                    $string = str_replace(array('-', '_'), array('+', '/'), $string);
                                    $string = base64_decode($string);
                                    for ($i = 0; $i < strlen($string); $i++) {
                                        $char = substr($string, $i, 1);
                                        $keychar = substr($key, ($i % strlen($key)) - 1, 1);
                                        $char = chr(ord($char) - ord($keychar));
                                        $result.=$char;
                                    }
                                    return $result;
                                }

                                $clave_encriptada = $_GET['regedi'];

                                //$clave_encriptada = str_replace(array('-', '_'), array('+', '/'), $clave_encriptada);
                                $cadena_desencriptada = decrypt($clave_encriptada, $clave_encriptacion2);

                                $array_cadena = explode('-', $cadena_desencriptada);
                                $clavecilla = fn_filtro($con,$array_cadena[0]);
                                $id_usuario = fn_filtro($con,$array_cadena[1]);
                            }
                            ?>
                            <?php if ($clavecilla == "nwt") { ?>
                                <input name="idrec" type="hidden" id="idrec" value="<?php echo $_GET['regedi']; ?>">
                                <?php
                                $usuario_consulta = mysqli_query($con, "SELECT ID,usuario FROM $tbn11 WHERE ID='$id_usuario' ") or die("No se pudo realizar la consulta a la Base de datos");

                                while ($listrows = mysqli_fetch_array($usuario_consulta)) {
                                    $ID = $listrows[ID];
                                    $usuario = $listrows[usuario];
                                }
                                ?>

                            <?php } ?>
                            <?php if ($_GET['idpr'] != "") { ?>
                                <input name="npdr" type="hidden" id="npdr" value="<?php echo $_GET['idpr']; ?>">
                            <?php } ?>

                            <div class="control-group">
                                <div class="controls">
                                    <input type="email" class="form-control" placeholder="Su correo electronico" id="email" required  data-validation-required-message="Por favor, ponga su correo electronico" />
                                </div>
                            </div> 	
                            <?php if ($usuario != "") { ?>
                                <div class="control-group">
                                    <div class="controls">
                                        <label for="usuario">Su nombre de usuario es:</label>
                                        <input type="text" class="form-control" id="name" value="<?php echo "$usuario"; ?>"   data-validation-required-message="Su usuario"  />
                                        <p class="help-block">Debe de usar caracteres alfanumericos y no dejar espacios en blanco </p>
                                    </div>
                                </div> 
                            <?php } else { ?> <div class="control-group">
                                    <div class="controls">
                                        <label for="usuario">Ponga el nombre de usuario que quiere usar</label>
                                        <input type="text" required class="form-control" id="name"  placeholder="Ponga el nombre de usuario que quiere usar"  data-validation-required-message="Su usuario" />
                                        <p class="help-block">Debe de usar caracteres alfanumericos y no dejar espacios en blanco </p>
                                    </div>
                                </div> 	
                            <?php } ?>
                            <div class="control-group">
                                <div class="controls">
                                    <input type="password" class="form-control" placeholder="Escoja su password" id="pass" name="pass" required  data-validation-required-message="El password es un dato requerido" />
                                    <p class="help-block"></p>
                                </div>
                            </div> 	


                            <div class="control-group">
                                <div class="controls">
                                    <input type="password" class="form-control" placeholder="Repita su password" id="pass2"  name="pass2"  data-validation-match-match="pass" required  data-validation-required-message="El password es un dato requerido" data-validation-match-message="El password tiene que ser igual" />
                                    <p class="help-block"></p>
                                </div>
                            </div> 	



                            <button type="submit" class="btn btn-primary pull-right">Enviar</button><br />
                        </div>

                        <div id="success"> </div> <!-- mensajes -->   
                    </form>



                </fieldset>


            </div> 
            <!--
            ================================= ventana modal
            -->    
            <div class="modal fade" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal" >x</a>
                            <!--    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                            <h4 class="modal-title">Complete para registrarse o recuperar su contraseña</h4>
                        </div>

                        <div class="modal-body">
                            ventana modal
                        </div>



                    </div>
                </div>
            </div>
            <!--
            ===========================  fin ventana modal
            -->


            <div id="footer" class="row">
                <?php include("temas/$tema_web/pie_com.php"); ?>
            </div>





        </div>  

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
        <script src="../js/jquery-1.9.0.min.js"></script>
        <!--<script src="js/jquery.validate.js"></script>-->
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script src="../js/jqBootstrapValidation.js"></script>
        <script src="crea_contrasena.js"></script>

    </body>
</html>