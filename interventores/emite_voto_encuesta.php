<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
require_once("../config/config.inc.php");
require_once("../inc_web/conexion.php");
include('seguri_inter.php'); 
require ("../basicos_php/funcion_control_votacion.php");
//require ("../basicos_php/basico.php");
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title><?php echo "$nombre_web"; ?></title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content=" ">
    <meta name="author" content=" ">
    <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png"> 
    
    
    
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">
  
  </head>
  <body>
  <!-- NAVBAR
================================================== -->

    
<!-- END NAVBAR
================================================== -->

 <div class="container">
 
    <!-- cabecera
    ================================================== -->
      <div class="page-header">
      <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
	  </div>
      
    <!-- END cabecera
    ================================================== -->
      <?php // include("../votacion/caja_mensajes_1.php"); ?>

       <div class="row">
        
		<div class="col-md-2" >             
			<?php  include("menu.php"); ?>    
        </div>
        <div class="col-md-10">
        

                   
                   <!--Comiezo-->
                   <?php 

 
if(ISSET($_POST["add_voto"])){
	
$id_votante = $_SESSION['ID'];///ver si se puede borrar
$idvot=fn_filtro_numerico($con,$_POST['id_vot']); ///ver si se puede borrar y meter el post directamente
$valores=fn_filtro($con,$_POST['valores']);

       for ($i = 0; $i < $_SESSION['numero_inter']; $i++) {  //metemos el dato de os interventores que añaden este voto
            $id_inter = "ID_inter_" . $i;
            $inter.=$_SESSION[$id_inter];
            $inter.= "_";
             }
       $inter = trim($inter, '_'); //quitamos el ultimo _


			reset ($_POST);
			foreach ($_POST as  $k => $v) $a[] = $v;
			$datos=count ($a)-7;
			
	if($datos==0){
	echo "<div class=\"alert alert-danger\">
   <strong>Hay un error, no ha seleccionado ninguna opción <a href=\"vota_encuesta.php?idvot=$idvot\">volver</a></strong></div>";
	}
	else{
			


$sql3 = "SELECT  seguridad,nombre_votacion FROM $tbn1 WHERE ID ='$idvot' ";
$resulta3 = mysqli_query($con, $sql3) or die("error: ".mysqli_error());
	
	while( $listrows3 = mysqli_fetch_array($resulta3) ){ 
	$seguridad  = $listrows3[seguridad];
	$nombre_votacion = $listrows3[nombre_votacion];
}



if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
       $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    elseif (isset($_SERVER['HTTP_VIA'])) {
       $ip = $_SERVER['HTTP_VIA'];
    }
    elseif (isset($_SERVER['REMOTE_ADDR'])) {
       $ip = $_SERVER['REMOTE_ADDR'];
    }
	
$forma_votacion=3;


/////////////////////////// si podemos procesar el formulario
//if(!$error) {


if($idvot!="" and $inter!=""){
		$codi = hash("sha512", $clave_seg);

			$i = 0;
		
			while ($i < $datos) {
			$val = fn_filtro($con,$a[$i]);
			
 			$vot=1;
				
		
					
						//$datos_votado.="Orden $voto  | Identificador candidato -->  $val |  Valor voto ---  $vot"."<br/>"; //cojemos el array de votos para enviar por correo si es necesario
		 				
						$datos_votado.=$val."-".$vot.","; //cojemos el array de votos para enviar por correo si es necesario
			
			$i++;
		}	 
		
					//////	identofocador unico		
			$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
			$cad = "";
			for($b=0;$b<4;$b++) {
			$cad .= substr($str,rand(0,62),1);
			}
				$time = microtime(true);
				$timecad=$time.$cad;
				$res_id = hash("sha256", $timecad);
			/// finidentificador unico		
					
		$datos_del_voto = trim($datos_votado, ','); ///quitamos la ultima coma de la cadena para meterlo en la bbdd
								 
		$fecha_env = date("Y-m-d H:i:s");
        $especial = 1; //metemos en la base de datos que ese voto ha sido incluido por interventores

        $provincia=999; // incluimos una provincia inexistente 
        $insql = "insert into $tbn10 (ID,voto,id_candidato,id_provincia,id_votacion,codigo_val,vote_id,position,otros,especial,incluido) values (\"$res_id\",\"$vot\",\"$datos_del_voto\",\"$provincia\",\"$idvot\",\"$codi\",\"$vote_id\",\"$position\",\"$nulo_blanco\",\"$especial\",\"$inter\")";
		//$insql = "insert into $tbn10 (ID,voto,id_candidato,id_provincia,id_votacion,codigo_val) values (\"$res_id\",\"$vot\",\"$datos_del_voto\",".$provincia.",\"$idvot\",\"$codi\")";
		$mens="mensaje añadido";
		$result =db_query($con, $insql,$mens);			
					
		if(!$result){
			echo "<div class=\"alert alert-danger\">
   			<strong> UPSSS!!!!<br/>esto es embarazoso, hay un error y su votacion no ha sido registrada  </strong> </div><strong>";
		
			}
		if($result){   
		
		// ALTA del usuario en la lista de los que han votado para que no pueda volver a votar
/*					$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
					$cad = "";
					for($i=0;$i<6;$i++) {
						$cad .= substr($str,rand(0,62),1);
					}
					$time = microtime(true);
					$timecad2 = $time.$cad;
					$user_id = hash("sha256", $timecad2);
					$fecha_env = date("Y-m-d H:i:s");	
				$insql = "insert into $tbn2 (ID,id_provincia,id_votacion,id_votante,fecha,tipo_votante,ip,forma_votacion) values (\"$user_id\",".$_SESSION['localidad'].",\"$idvot\",".$_SESSION['ID'].",\" $fecha_env\",\" $tipo_votante\",\" $ip\",\" $forma_votacion\")";
				$mens="<br/>¡¡¡ATENCION!!!!, el voto ha sido registrado , pero el usuario no ha sido bloqueado <br> el ID de usuario es:".$_SESSION[ID];
				$result =db_query($con,$insql,$mens);	*/
				
				
				////metemos el voto en un txt y lo registramos como medida de seguridad
				$Pollname=md5($idvot);
				$file=$FilePath.$Pollname."_ballots.txt";
				$fh = fopen($file, 'a');				
				fwrite($fh, $datos_votado . PHP_EOL)or die("Could not write file!");				
				fclose($fh);  //  Cerramos el fichero 

				$file2=$FilePath.$Pollname."_tally.txt";
				$fh2 = fopen($file2, 'r+');			
				$búfer = fgets($fh2,100);	
				$partes = explode("|", $búfer);
				$partes1=$partes[1]+1;
				$partes=$partes[0]."|".$partes1;
				fseek($fh2, 0);
				fwrite($fh2, $partes)or die("Could not write file!");				
				fclose($fh2);  //  Cerramos el fichero 
		
			//// metemos el dato de que ha votado en la base de datos 
		  // echo "Ha sido recogido el voto de: ".$_SESSION['nombre_usu']."<br/>";

					///// metemos una copia encriptada del voto
					//// primero sacamos los datos de la tabla temporal para hacer una cadena quemeteremos tambien encriptada
					$sql = "SELECT 	ID, datos  FROM $tbn20 where id_votacion=\"$Pollname\" ORDER BY ID DESC";
					$mens="Error en la consulta a la tabla temporal";
					$result = db_query($con, $sql,$mens);
					if ($row = mysqli_fetch_array($result)){
						mysqli_field_seek($result,0);
						do {
						$cadena.=$row[1]."_";  ////generamos la caena que vamos a meter en la tabla de votos de seguridad
						$id_borrado=$row[0];
						}
					while ($row = mysqli_fetch_array($result));
					}
					
					
					$cadena = trim($cadena, '_'); // quitamos de la cadena el ultimo _
					//$id_borrado=$id_borrado-4; ///miramos el id del ultimo registro de la tabla temporal y descontamos 4 para su posterior borrado
					
					$borrado = "DELETE FROM $tbn20 WHERE ID=".$id_borrado." " ;
					$mens=" error en el borrado de ".$id_borrado;
					$result = db_query($con, $borrado,$mens);
	
					$cadena_temp=$res_id."+".$datos_del_voto; //esta variable sirve para esta querry y para la querry de la tabla temporal siguiente
					
					/////// miramos si hay que encriptar la cadena de voto

					if ($encripta=="si"){
						
						/*require_once "Crypt/AES.php";*/

						//Function for encrypting with RSA
						
							// hacemos una busqueda y cargamos la clave publica
							$result=mysqli_query($con,"SELECT clave_publica FROM $tbn21  WHERE id_votacion='$idvot' ORDER by orden");
 						 	if ($row = mysqli_fetch_array($result)){
								mysqli_field_seek($result,0);
								do {
  									$clave_publica=$row[0];
							
									$cadena=rsa_encrypt($cadena, $clave_publica);
		
									}
							   while ($row = mysqli_fetch_array($result));
 
           					 }
						}
					//metemos los datos codificados en la bbdd
					$timecad2 = $time.$cad;
					$ident=md5($timecad2);
					
					$shadatovoto= hash('sha256', $cadena_temp);					
					$insql1 = "insert into $tbn19 (ID,voto,cadena,id_votacion) values (\"$ident\",\"$shadatovoto\",\"$cadena\",\"$Pollname\")";
					$mens1 = "ERROR en el añadido voto encriptado ";
					$result = db_query($con,  $insql1,$mens1);
					
					//metemos el dato en la tabla temporal
					$cadena_temp=$res_id."+".$datos_del_voto;
					$insql2 = "insert into $tbn20 (datos,fecha,id_votacion) values (\"$cadena_temp\",\"$time\",\"$Pollname\")";
					$mens2 = "ERROR en el añadido  del voto tabla temporal";
					$result = db_query($con,  $insql2,$mens2);

				
							
				?><!--si todo va bien damos las gracias por participar-->
    <div class="alert alert-success">   <h3  align="center">Ha sido incluido el voto</h3> </div>
   
   <a href="vota_encuesta.php?idvot=<?php echo $idvot; ?>" class="btn btn-primary btn-block">Incluir otro voto</a>
				<?php 
			}
		}
  	}
  }

	?>
                   
                   
                   <!--Final-->
        
  
        </div>
        
         
      
  </div>
 

  <div id="footer" class="row">
   <?php  include("../votacion/ayuda.php"); ?>
  <?php  include("../temas/$tema_web/pie.php"); ?>
   </div>
 </div>  
   
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->    
<script src="../js/jquery-1.9.0.min.js"></script>
	<script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
   
  </body>
</html>