<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
require_once("../config/config.inc.php");
require_once("../inc_web/conexion.php");
include('../inc_web/seguri.php');
ini_set('memory_limit', '3064M'); //para test de memoria
ini_set('max_execution_time', 900); //900 seconds = 15 minutes
ob_start();
?>

<!DOCTYPE html>
<html lang="es"><head>
        <meta charset="utf-8">
        <title><?php echo "$nombre_web"; ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content=" ">
        <meta name="author" content=" ">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png"> 



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>


        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">


                <div class="col-md-12">
                    <!--Comiezo--><h1><?php echo "$nombre_votacion"; ?></h1>
                    <?php echo "$resumen"; ?>

                </div>
            </div>


            <h2>Candidatos u opciones de esta votación</h2>  

            <table class="table table-striped">
                <tr>

                    <th width="10%">Identificador</th>
                    <th width="80%">Nombre</th>

                </tr>   
                <?php
// sacamos los datos del array

                $sql2 = "SELECT ID, nombre_usuario,sexo  FROM $tbn7 WHERE id_votacion=" . $idvot . " ";
                $result2 = mysqli_query($con, $sql2);
                if ($row2 = mysqli_fetch_array($result2)) {
                    mysqli_field_seek($result2, 0);

                    do {
                        ?>	 
                        <tr>
                            <td><?php echo $row2[0]; ?></td>
                            <td><?php echo $row2[1]; ?></td>



                        </tr>

                        <?php
                    } while ($row2 = mysqli_fetch_array($result2));
                }
                ?>

            </table>      



            <h2>Lista de todos los votos de esta votacion</h2>  
            <p> El primer dato es el identificador unico, si has guardado el tuyo puedes comprobar que esta en la lista</p>     
            <p> El segundo dato corresponde al identificador del candidato u opcion y la puntuacion asignada</p>
            <p> Los distintos candidatos u opciones estan separados por comas</p>
            <p> El ultimo parametro corresponde al estado de la comprobacion del voto con la copia de seguridad encriptada</p>
            

			<?php
            $Pollname = md5($idvot); //encriptamos el id de la votacion 
            /* nuevo */
            
            $timeStart = microtime(true);
            $i = 1;
            $sql = "SELECT ID, id_candidato FROM $tbn10 WHERE id_votacion = '$idvot' and especial=0  ";
            $result = mysqli_query($con, $sql);
            if ($row = mysqli_fetch_array($result)) {
                mysqli_field_seek($result, 0);
				echo "<table  class=\"table table-hover\">
				  <tr>
					<th width=\"3%\" scope=\"row\">#</th>
					<td width=\"55%\">HASH</td>
					<td width=\"37%\">VOTOS</td>
					<td width=\"5%\">&nbsp;</td>
				  </tr>";
                do {

                    $cadena_temp = $row[0] . "+" . $row[1];
                    $shadatovoto = hash('sha256', $cadena_temp);

                    $conta = "SELECT id  FROM $tbn19 WHERE id_votacion = \"" . $Pollname . "\"  and voto = \"" . $shadatovoto . "\" ";
                    $result_cont = mysqli_query($con, $conta);
                    $quants = mysqli_num_rows($result_cont);
                    if ($quants == 1) {
                        $resulta = "OK";
                    } else {
                        $resulta = "ERROR";
                    }

                    echo "<tr><th scope=\"row\">".$i++ . " </td><td> " . $row[0] . " </td><td> " . $row[1] . " </td><td> " . $resulta . "</td></tr>";
                } while ($row = mysqli_fetch_array($result));
           
		  	echo" </table>";
		    }
            ?>



            <?php
            $i = 1;
            $sql = "SELECT ID, id_candidato,incluido FROM $tbn10 WHERE id_votacion = '$idvot' and especial=1  ";
            $result = mysqli_query($con, $sql);
            if ($row = mysqli_fetch_array($result)) {
                ?>
                <h2>Votos presenciales incluidos por los interventores</h2>
                <p>El ultimo parametro corresponde a los identificadores de los interventores que han incluido el voto</p>
                <?php
				echo "<table  class=\"table table-hover\">
				  <tr>
					<th width=\"3%\" scope=\"row\">#</th>
					<td width=\"45%\">HASH</td>
					<td width=\"32%\">VOTOS</td>
					<td width=\"25%\">INTERVENTORES</td>
				  </tr>";	
                mysqli_field_seek($result, 0);

                do {

                    echo "<tr><th scope=\"row\">".$i++ . "  </td><td>  " . $row[0] . " </td><td> " . $row[1] . " </td><td> " . $row[2] . "</td></tr>";
                } while ($row = mysqli_fetch_array($result));
				echo" </table>";
            }
            ?>


            <!--fin nuevo bloque-->



            <!--Final-->

            <!--  <div class="col-md-3">
            <?php // include("lateral_derecho.php"); ?>              
            </div>-->

            <p>&nbsp;</p>
            <p>&nbsp;</p>

            <div id="footer" class="row">
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>  

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->    
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script> 


    </body>
</html>
<?php
$contenido = ob_get_contents();
ob_end_flush();
$archivo = $FileRec . $idvot . "_list.html";
$crear = fopen($archivo, "w");
$grabar = fwrite($crear, $contenido);
fclose($crear);
?>
<?php
$timeEnd = microtime(true);
$timeElapsed = $timeEnd - $timeStart;
echo "<br/>";

printf("Memory used: %s kB\n", memory_get_peak_usage() / 1024);
printf("Total time: %s s\n", $timeElapsed);
/* fin nuevo */
?>