<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
require_once("../config/config.inc.php");
require_once("../inc_web/conexion.php");
include('../inc_web/seguri.php');
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><?php echo "$nombre_web"; ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content=" ">
        <meta name="author" content=" ">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png"> 



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">

                <div class="col-md-2" >             

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>


                <div class="col-md-7"><!--Comiezo-->

                    <?php
                    $idvot = fn_filtro_numerico($con, $_POST['id_vot']);
                    $clave_seg = fn_filtro($con, $_POST['clave_seg']);
                    $codigo_val = hash("sha512", $clave_seg);
                    ?>




                    <h1>Esto ha votado en   "<?php echo "$nombre_votacion"; ?>"</h1>

                    <div>
                        <?php
                        // Votos en blanco
                        $sql = "select vote_id from $tbn10 WHERE id_votacion = '$idvot' and codigo_val LIKE '$codigo_val' and otros=2";
                        $result = mysqli_query($con, $sql);
                        if ($row = mysqli_fetch_array($result)) {
                            //$blancos = mysqli_num_rows($result); // obtenemos el número de filas  
                            echo " <ul class=\"candidates_res\"><li class=\"ne\">Su voto es en blanco </li></ul>";
                            $finalizar = TRUE;
                        }
                        // Votos en nulos
                        $sql = "select vote_id from $tbn10 WHERE id_votacion = '$idvot' and  codigo_val LIKE '$codigo_val' and otros=1";
                        $result = mysqli_query($con, $sql);
                        if ($row = mysqli_fetch_array($result)) {
                            echo " <ul class=\"candidates_res\"><li class=\"ne\">Su voto es Nulo</li></ul>";
                            $finalizar = TRUE;
                            // $nulos = mysqli_num_rows($result); // obtenemos el número de filas
                        }
                        ?>  

                        <?php
                        if ($finalizar != TRUE) {
                            $sql = "SELECT ID, id_candidato FROM $tbn10 WHERE  id_votacion = '$idvot' and codigo_val LIKE '$codigo_val'  ";
                            $result = mysqli_query($con, $sql);
                            $row = mysqli_fetch_row($result);
                            $arr = explode(',', $row[1]);
                            foreach ($arr as $val) {
                                $arr2 = explode('-', $val);
                                foreach ($arr2 as $val2) {
                                    
                                }


                                $sql2 = "SELECT  nombre_usuario,sexo,imagen_pequena FROM $tbn7 WHERE ID = '" . $arr2[0] . "'  ";
                                $result2 = mysqli_query($con, $sql2);
                                if ($row2 = mysqli_fetch_array($result2)) {
                                    mysqli_field_seek($result2, 0);
                                    ?>
                                    <div class="row">
                                        <div id="table-men-c" class="col-md-12">
                                            <ul class="candidates men ">
                                                <?php
                                                do {
                                                    ?>
                                                    <?php ?>
                                                    <li data-id="<?php echo "$row2[0]" ?>" "> 
                                                        <span class="name"><?php if ($row2['imagen_pequena'] == "") { ?><?php } else { ?><img src="<?php echo $upload_cat; ?>/<?php echo $row2['imagen_pequena']; ?>" alt="<?php echo $row2['nombre_usuario']; ?> " width="60" height="60"  /> <?php } ?><?php echo "$row2[0]" ?> </span>

                                                    </li>

                                                    <?php
                                                } while ($row2 = mysqli_fetch_array($result2));
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>







                        <!--Final-->


                    </div>

                    <div class="col-md-3">

                        <?php // include("lateral_derecho.php");  ?>              
                    </div>

                </div>


                <div id="footer" class="row">
                    <?php include("../votacion/ayuda.php"); ?>
                    <?php include("../temas/$tema_web/pie.php"); ?>
                </div>
            </div>  

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->    
            <script src="../js/jquery-1.9.0.min.js"></script>
            <script type='text/javascript' src='../js/jquery.checkboxes.min.js'></script>
            <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
            <script type="text/javascript">
<!-- limpiamos la carga de modal para que no vuelva a cargar lo mismo -->
                $('#ayuda_contacta').on('hidden.bs.modal', function() {
                    $(this).removeData('bs.modal');
                });
            </script>
    </body>
</html>