<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 0;
if ($nivel_acceso = $_SESSION['usuario_nivel']) {
    header("Location: $redir?error_login=5");
    exit;
}
include("../basicos_php/modifika_config.php");

$file = "../config/config.inc.php";

if (ISSET($_POST["modifika_general"])) {
    $com_string = "email_env=\"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_error"])) {
    $com_string = "email_error=\"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_control"])) {
    $com_string = "email_control=\"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_tecnico"])) {
    $com_string = "email_error_tecnico=\"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_sistema"])) {
    $com_string = "email_sistema=\"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_url"])) {
    $com_string = "url_de_la_web=\"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_nombre"])) {
    $com_string = "nombre_sistema=\"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_asunto"])) {
    $com_string = "asunto=\"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_mens_error"])) {
    $com_string = "asunto_mens_error=\"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_web"])) {
    $com_string = "nombre_web=\"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_tema"])) {
    $com_string = "tema_web=\"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_user_mail"])) {
    $com_string = "user_mail=\"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_host_smtp"])) {
    $com_string = "host_smtp=\"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}

if (ISSET($_POST["modifika_pass_mail"])) {
    $com_string = "pass_mail=\"";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_puerto_mail"])) {
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_mail_sendmail"])) {
    $com_string = "mail_sendmail=";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_auth"])) {
    $com_string = "cfg_autenticacion_solo_local=";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_SMTPSecure"])) {
    $com_string = "mail_SMTPSecure=";
	if($_POST['valor']=="false"){
		$dato_viejo=fn_filtro_nodb($_POST['valor']);
		$nuevo_dato="\"".fn_filtro_nodb($_POST['direccion_general'])."\"";
	}
	if($_POST['valor']=="SSL" or $_POST['valor']=="TLS"){
		if($_POST['direccion_general']=="false"){
		$dato_viejo="\"".fn_filtro_nodb($_POST['valor'])."\"";	
		$nuevo_dato=fn_filtro_nodb($_POST['direccion_general']);
		}else{
		$dato_viejo="\"".fn_filtro_nodb($_POST['valor'])."\"";	
		$nuevo_dato="\"".fn_filtro_nodb($_POST['direccion_general'])."\"";
		}
	}
    $find = $com_string . $dato_viejo;
    $replace = $com_string . $nuevo_dato;
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_mail_SMTPAuth"])) {
    $com_string = "mail_SMTPAuth=";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_mail_IsHTML"])) {
    $com_string = "mail_IsHTML=";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
if (ISSET($_POST["modifika_versiones"])) {
    $com_string = "info_versiones=";
    $find = $com_string . fn_filtro_nodb($_POST['valor']);
    $replace = $com_string . fn_filtro_nodb($_POST['direccion_general']);
    find_replace($find, $replace, $file, $case_insensitive = true);
}
include('../config/config.inc.php');
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><?php echo "$nombre_web"; ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content=" ">
        <meta name="author" content=" ">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png"> 



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">
                <div class="col-md-2" >             

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-10">



                    <!--Comiezo-->
                  <h2>Constantes de configuración </h2>
                    <table width="100%" border="0"  class="table table-striped">
                        <tr>
                            <th width="39%" scope="row">&nbsp;</th>
                            <td width="5%">&nbsp;</td>
                            <td width="23%">&nbsp;</td>
                            <td width="33%">&nbsp;</td>
                        </tr>
                        <tr>
                            <th scope="row">Direccion de correo general</th>
                            <td width="5%">&nbsp;</td>
                            <td width="23%"><?php echo $email_env; ?></td>
                            <td width="33%">
                                <form name="form1" method="post" action=""> 
                                    <input name="direccion_general" type="email" autofocus required class="form-control" id="direccion_general"  placeholder="Direccion de correo" value="<?php echo $email_env; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $email_env; ?>" >
                                    <input type="submit" name="modifika_general" id="modifika_general" value="modificar" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row">Sitio al que enviamos los correos de los que tienen problemas y no estan en la bbdd, este correo es el usado si no hay datos en la bbdd de los contactos por provincias</th>
                            <td>&nbsp;</td>
                            <td><?php echo $email_error; ?></td>
                            <td><form name="form1" method="post" action=""> 
                                    <input name="direccion_general" type="email" autofocus required class="form-control" id="direccion_general"  placeholder="Direccion de correo" value="<?php echo $email_error; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $email_error; ?>" >
                                    <input type="submit" name="modifika_error" id="modifika_error" value="modificar" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row">Direccion que envia el correo para el control con interventores</th>
                            <td>&nbsp;</td>
                            <td><?php echo $email_control; ?></td>
                            <td><form name="form1" method="post" action=""> 
                                    <input name="direccion_general" type="email" autofocus required class="form-control" id="direccion_general"  placeholder="Direccion de correo" value="<?php echo $email_control; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $email_control; ?>" >
                                    <input type="submit" name="modifika_control" id="modifika_control" value="modificar" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row">Correo electronico del responsable tecnico</th>
                            <td>&nbsp;</td>
                            <td><?php echo $email_error_tecnico; ?></td>
                            <td><form name="form1" method="post" action=""> 
                                    <input name="direccion_general" type="email" autofocus required class="form-control" id="direccion_general"  placeholder="Direccion de correo" value="<?php echo $email_error_tecnico; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $email_error_tecnico; ?>" >
                                    <input type="submit" name="modifika_tecnico" id="modifika_tecnico" value="modificar" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row">Correo electronico del sistema, demomento incluido en el envio de errores de la bbdd</th>
                            <td>&nbsp;</td>
                            <td><?php echo $email_sistema; ?></td>
                            <td><form name="form1" method="post" action=""> 
                                    <input name="direccion_general" type="email" autofocus required class="form-control" id="direccion_general"  placeholder="Direccion de correo" value="<?php echo $email_sistema; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $email_sistema; ?>" >
                                    <input type="submit" name="modifika_sistema" id="modifika_sistema" value="modificar" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td>&nbsp;</td>
                            <td>&nbsp; </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <th scope="row">Tipo de envio de correo</th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($correo_smtp = true) {
                                    echo " SMTP";
                                } else {
                                    echo "Mail";
                                }
                                ?>
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Usuario de correo </th>
                            <td>&nbsp;</td>
                            <td><?php echo $user_mail; ?></td>
                            <td><form name="form1" method="post" action=""> 
                                    <input name="direccion_general" type="text"  autofocus required class="form-control" id="direccion_general"  value="<?php echo $user_mail; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $user_mail; ?>" >
                                    <input type="submit" name="modifika_user_mail" id="modifika_user_mail" value="modificar" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"> Host del correo</th>
                            <td>&nbsp;</td>
                            <td><?php echo $host_smtp; ?></td>
                            <td><form name="form1" method="post" action=""> 
                                    <input name="direccion_general" type="text"  autofocus required class="form-control" id="direccion_general"  value="<?php echo $host_smtp; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $host_smtp; ?>" >
                                    <input type="submit" name="modifika_host_smtp" id="modifika_host_smtp" value="modificar" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row">Contraseña</th>
                            <td>&nbsp;</td>
                            <td>*******</td>
                            <td><form name="form1" method="post" action=""> 
                                    <input name="direccion_general"  type="password"  autofocus required class="form-control" id="direccion_general"  value="<?php echo $pass_mail; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $pass_mail; ?>" >
                                    <input type="submit" name="modifika_pass_mail" id="modifika_pass_mail" value="modificar" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row">Puerto del servidor</th>
                            <td>&nbsp;</td>
                            <td><p>&nbsp;</p>
                                <p><?php echo $puerto_mail; ?></p></td>
                            <td><form name="form1" method="post" action="">
                                    <input name="direccion_general" type="text"  autofocus required class="form-control" id="direccion_general"  placeholder="Direccion de correo" value="<?php echo $puerto_mail; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $puerto_mail; ?>" >
                                    <input type="submit" name="modifika_puerto_mail" id="modifika_puerto_mail" value="modificar" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"><span class="col-sm-4 control-label">SMTPSecure</span></th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($mail_SMTPSecure == false) {
                                    echo "Deshabilitado";
                                    $check1 = "checked=\"CHECKED\"";
                                } else if($mail_SMTPSecure == "TLS") {
                                    echo"TLS";
                                    $check2 = "checked=\"CHECKED\"";
                                }
								else if($mail_SMTPSecure == "SSL") {
                                    echo"SSL";
                                    $check3 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action=""> 
                                    <label>
                                      <input name="direccion_general" type="radio" id="direccion_general_0" value="false" <?php echo $check1 ?>>
                                      Deshabilitado</label>
                                    <label>
                                      <input name="direccion_general" type="radio" id="direccion_general_2" value="TLS" <?php echo $check2 ?>>
                                      TLS</label>
                                    <label>
                                      <input name="direccion_general" type="radio"  id="direccion_general_1" value="SSL" <?php echo $check3 ?>>
                                      SSL</label>
                                  
                                    <input name="valor" type="hidden" id="valor" value="<?php
                                if ($mail_SMTPSecure == false) {
                                    echo "false";
                                } else if($mail_SMTPSecure == "TLS") {
                                    echo "TLS";
                                }
								else if($mail_SMTPSecure == "SSL") {
                                    echo"SSL";
                                }
                                    ?>" >
                                    <input type="submit" name="modifika_SMTPSecure" id="modifika_SMTPSecure" value="modificar" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>
                        <tr>
                          <th scope="row"><span class="col-sm-4 control-label">Autentificación por SMTP (SMTPAuth)</span></th>
                          <td>&nbsp;</td>
                          <td><?php
                                if ($mail_SMTPAuth == true) {
                                    echo "Habilitado";
                                    $check4 = "checked=\"CHECKED\"";
                                } else {
                                    echo"Deshabilitado";
                                    $check5 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                          <td><form name="form1" method="post" action="">
                            <label>
                              <input name="direccion_general" type="radio" id="direccion_general_2" value="true" <?php echo $check4 ?>>
                              Habilitado
                            </label>
                            <label>
                              <input type="radio" name="direccion_general" id="direccion_general_3" value="false" <?php echo $check5 ?>>
                              Deshabilitado</label>
                            <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($mail_SMTPAuth == true) {
                                        echo "true";
                                    } else {
                                        echo"false";
                                    }
                                    ?>" >
                            <input type="submit" name="modifika_mail_SMTPAuth" id="modifika_mail_SMTPAuth" value="modificar" class="btn btn-primary pull-right" >
                          </form></td>
                        </tr>
                        <tr>
                          <th scope="row"><span class="col-sm-4 control-label">Envio html o texto plano(IsHTML)</span></th>
                          <td>&nbsp;</td>
                          <td><?php
                                if ($mail_IsHTML == true) {
                                    echo "Habilitado";
                                    $check6 = "checked=\"CHECKED\"";
                                } else {
                                    echo"Deshabilitado";
                                    $check7 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                          <td><form name="form1" method="post" action="">
                            <label>
                              <input name="direccion_general" type="radio" id="direccion_general_4" value="true" <?php echo $check6 ?>>
                              Habilitado</label>
                            <label>
                              <input type="radio" name="direccion_general" id="direccion_general_5" value="false" <?php echo $check7 ?>>
                              Deshabilitado</label>
                            <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($mail_IsHTML == true) {
                                        echo "true";
                                    } else {
                                        echo"false";
                                    }
                                    ?>" >
                            <input type="submit" name="modifika_mail_IsHTML" id="modifika_mail_IsHTML" value="modificar" class="btn btn-primary pull-right" >
                          </form></td>
                        </tr>
                        <tr>
                          <th scope="row">Forma de envio de correo</th>
                          <td>&nbsp;</td>
                          <td><?php
                                if ($mail_sendmail == true) {
                                    echo "IsSendMail";
                                    $check8 = "checked=\"CHECKED\"";
                                } else {
                                    echo"IsSMTP";
                                    $check9 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                          <td><form name="form1" method="post" action="">
                            <label>
                              <input name="direccion_general" type="radio" id="direccion_general_6" value="false" <?php echo $check9 ?>>
                              IsSMTP</label>
                            <label>
                              <input type="radio" name="direccion_general" id="direccion_general_7" value="true" <?php echo $check8 ?>>
                              IsSendMail</label>
                            <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($mail_sendmail == true) {
                                        echo "true";
                                    } else {
                                        echo"false";
                                    }
                                    ?>" >
                            <input type="submit" name="modifika_mail_sendmail" id="modifika_mail_sendmail" value="modificar" class="btn btn-primary pull-right" >
                          </form></td>
                        </tr>
                        <tr>
                            <th colspan="4" scope="row"><a data-toggle="modal"  href="../admin/comprueba_correo.php" data-target="#ayuda_contacta"  class="btn btn-success pull-right">Comprobar configuración de correo</a></th>
                        </tr>
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <th scope="row">url de la web de la votacion,(ojo  sin barra final)</th>
                            <td>&nbsp;</td>
                            <td><?php echo $url_vot; ?></td>
                            <td><form name="form1" method="post" action=""> 
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $url_vot; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $url_vot; ?>" >
                                    <input type="submit" name="modifika_url" id="modifika_url" value="modificar" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"> Nombre del sistema cuando se envia el correo de recupercion de clave</th>
                            <td>&nbsp;</td>
                            <td><?php echo $nombre_sistema; ?></td>
                            <td><form name="form1" method="post" action=""> 
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $nombre_sistema; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $nombre_sistema; ?>" >
                                    <input type="submit" name="modifika_nombre" id="modifika_nombre" value="modificar" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row">Asunto del correo para recuperar la contraseña</th>
                            <td>&nbsp;</td>
                            <td><?php echo $asunto; ?></td>
                            <td><form name="form1" method="post" action=""> 
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $asunto; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $asunto; ?>" >
                                    <input type="submit" name="modifika_asunto" id="modifika_asunto" value="modificar" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row">asunto del mensaje de correo cuando hay problemas de acceso</th>
                            <td>&nbsp;</td>
                            <td><?php echo $asunto_mens_error; ?></td>
                            <td><form name="form1" method="post" action=""> 
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $asunto_mens_error; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $asunto_mens_error; ?>" >
                                    <input type="submit" name="modifika_mens_error" id="modifika_mens_error" value="modificar" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row">Nombre del sitio web</th>
                            <td>&nbsp;</td>
                            <td><?php echo $nombre_web; ?></td>
                            <td><form name="form1" method="post" action=""> 
                                    <input name="direccion_general" type="text" autofocus required class="form-control" id="direccion_general"  value="<?php echo $nombre_web; ?>" >
                                    <input name="valor" type="hidden" id="valor" value="<?php echo $nombre_web; ?>" >
                                    <input type="submit" name="modifika_web" id="modifika_web" value="modificar" class="btn btn-primary pull-right" >
                                </form></td>
                        </tr>
                        <tr>
                            <th scope="row"> Nombre del tema (carpeta donde se encuentra)</th>
                            <td>&nbsp;</td>
                            <td><?php echo $tema_web; ?></td>
                            <td><form name="form1" method="post" action=""> 

                                    <?php
                                    $carpeta = "../temas"; //ruta actual
                                    if (is_dir($carpeta)) {
                                        if ($dir = opendir($carpeta)) {
                                            while (($archivo = readdir($dir)) !== false) {
                                                if ($archivo != '.' && $archivo != '..' && $archivo != '.htaccess' && $archivo != 'index.html' && $archivo != 'index.php') {

                                                    if ($archivo == $tema_web) {
                                                        $check = "selected=\"selected\" ";
                                                    } else {
                                                        $check = "";
                                                    }
                                                    $lista .="<option value=\"" . $archivo . "\" $check > " . $archivo . "</option>";
                                                    $lista.=$archivo;
                                                }
                                            }
                                            closedir($dir);
                                        }
                                    }
                                    ?>
                                    <select name="direccion_general" class="form-control"  id="direccion_general" >
                                        <?php echo "$lista"; ?>
                                    </select>


                                    <input name="valor" type="hidden" id="valor" value="<?php echo $tema_web; ?>" >
                                    <input type="submit" name="modifika_tema" id="modifika_tema" value="modificar" class="btn btn-primary pull-right" >
                                </form>



                            </td>
                        </tr> 
                        <tr>
                            <th scope="row">&nbsp;</th>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                             <th scope="row">Autenfificación federada</th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($cfg_autenticacion_solo_local == true) {
                                    echo "Solo Local";
                                    $check3 = "checked=\"CHECKED\"";
                                } else {
                                    echo"Autentificación federada";
                                    $check4 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action=""> 
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_0" value="false" <?php echo $check4 ?>>
                                        Autentificación federada</label>

                                    <label>
                                        <input type="radio" name="direccion_general" id="direccion_general_1" value="true" <?php echo $check3 ?>>
                                        Solo Local</label>

                                    <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($cfg_autenticacion_solo_local == true) {
                                        echo "true";
                                    } else {
                                        echo"false";
                                    }
                                    ?>" >
                                    <input type="submit" name="modifika_auth" id="modifika_auth" value="modificar" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>
                        <tr>
                             <th scope="row"><a name="avisos"></a>Avisos de actualizaciones</th>
                            <td>&nbsp;</td>
                            <td><?php
                                if ($info_versiones == true) {
                                    echo "Habilitado";
                                    $check5 = "checked=\"CHECKED\"";
                                } else {
                                    echo"Deshabilitado";
                                    $check6 = "checked=\"CHECKED\"";
                                }
                                ?></td>
                            <td><form name="form1" method="post" action=""> 
                                    <label>
                                        <input name="direccion_general" type="radio" id="direccion_general_0" value="false" <?php echo $check6 ?>>
                                        Deshabilitado</label>

                                    <label>
                                        <input type="radio" name="direccion_general" id="direccion_general_1" value="true" <?php echo $check5 ?>>
                                        Habilitado</label>

                                    <input name="valor" type="hidden" id="valor" value="<?php
                                    if ($info_versiones == true) {
                                        echo "true";
                                    } else {
                                        echo"false";
                                    }
                                    ?>" >
                                    <input type="submit" name="modifika_versiones" id="modifika_versiones" value="modificar" class="btn btn-primary pull-right" >
                                </form>
                            </td>
                        </tr>

                    </table>


                    <!--Final-->


                </div>      
            </div>


            <div id="footer" class="row">
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>  

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->    
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script type="text/javascript">
<!-- limpiamos la carga de modal para que no vuelva a cargar lo mismo -->
            $('#ayuda_contacta').on('hidden.bs.modal', function() {
                $(this).removeData('bs.modal');
            });

        </script>
    </body>
</html>