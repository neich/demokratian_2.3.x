<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 2;
if ($nivel_acceso <= $_SESSION['usuario_nivel']) {
    header("Location: $redir?error_login=5");
    exit;
}

$id = fn_filtro_numerico($con, $_GET['id']);
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><?php echo "$nombre_web"; ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content=" ">
        <meta name="author" content=" ">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png"> 



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../modulos/themes-jquery-iu/base/jquery.ui.all.css">
        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2" >             

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>
                <div class="col-md-10">

                    <!--Comiezo-->

                    <h1>BORRARDO AFILIADOS/SIMPATIZANTE</h1>
                    <p>&nbsp;</p>
                    <!----> 
                    <?php
                    $result = mysqli_query($con, "SELECT ID, 	id_provincia, 	nombre_usuario, 	apellido_usuario, 	nivel_usuario, 	nivel_acceso, 	correo_usuario, 	nif, 	id_ccaa, 	pass, 	tipo_votante ,	usuario, 	bloqueo, 	razon_bloqueo,imagen_pequena  FROM $tbn9 where id=$id");
                    $row = mysqli_fetch_row($result);
                    ?>  <form  class="well form-horizontal" >


                        <div class="form-group">       
                            <label for="nombre" class="col-sm-3 control-label">Nombre y apellidos</label>

                            <div class="col-sm-9">   
                                <?php echo "$row[2]"; ?>
                            </div></div>

                        <div class="form-group">       
                            <label for="correo" class="col-sm-3 control-label">Correo electronico</label>

                            <div class="col-sm-9"> <?php echo "$row[6]"; ?>
                            </div></div>

                        <div class="form-group">       
                            <label for="nombre" class="col-sm-3 control-label"> Nif </label>

                            <div class="col-sm-4">    <?php echo "$row[7]"; ?>
                            </div></div>


                        <div class="form-group">       
                            <label for="nombre" class="col-sm-3 control-label">provincia: </label>

                            <div class="col-sm-9"> 

                                <?php
                                $options = "select DISTINCT id, provincia from $tbn8  order by ID";
                                $resulta = mysqli_query($con, $options) or die("error: " . mysql_error($con));

                                while ($listrows = mysqli_fetch_array($resulta)) {
                                    $id_pro = $listrows[id];
                                    $name1 = $listrows[provincia];

                                    if ($id_pro == $row[1]) {
                                        echo "$row[1]";
                                    }
                                }
                                ?>

                            </div></div>


                        <div class="form-group">       
                            <label for="nombre" class="col-sm-3 control-label">Tipo </label>

                            <div class="col-sm-9"> 

                                <?php
                                if ($row[10] == 1) {
                                    echo "socio";
                                } else if ($row[10] == 2) {
                                    echo "	simpatizante verificado	";
                                } else {
                                    echo" simpatizante";
                                }
                                ?>

                            </div></div>



                        <div class="form-group">       
                            <label for="nombre" class="col-sm-3 control-label">Bloqueado </label>

                            <div class="col-sm-9">     


                                <?php
                                if ($row[12] == "si") {
                                    echo " SI  ";
                                } else {
                                    echo "NO";
                                }
                                ?>


                            </div></div>
                        <div class="form-group">       
                            <label for="nombre" class="col-sm-3 control-label">Razón Bloqueo </label>

                            <div class="col-sm-9"><?php echo "$row[13]"; ?>
                            </div></div> 
                        <p>&nbsp;</p>    
                    </form>



                    <?php
								///borramos la lista de votaciones donde ha participado
										$borrado_usuario_x_votacion ="DELETE FROM $tbn2 WHERE id_votante=" . $id . " ";
										$mens = "ERROR en el borrado de usuario_x_votacion";
										$result_usuario_x_votacion = db_query($con, $borrado_usuario_x_votacion, $mens);
										/*										
										if (!$result_usuario_x_votacion) {
										   $mens_error.=$mens."<br/>";
										}
										else{
										   $mens_ok.="Borrados la relacion de votantes de esta votación <br/>";
										}*/		
															
										//borramos debate_comentario con esta id
										$borrado_debate_comentario = "DELETE FROM $tbn12 WHERE id_usuario=" . $id . " ";
										$mens = "ERROR en el borrado de debate comentario";
										$result_borrado_debate_comentario = db_query($con, $borrado_debate_comentario, $mens);
										/*	
									   if (!$result_borrado_debate_comentario) {
										   $mens_error.=$mens."<br/>";
										}
										else{
										   $mens_ok.="Borrados los comentarios de este debate <br/>";
										}*/
										
										//borramos debate_votos con esta id
										$borrado_debate_votos =  "DELETE FROM $tbn14 WHERE id_votante=" . $id . "";
										$mens = "ERROR en el borrado de debate votos";
										$result_borrado_debate_votos = db_query($con, $borrado_debate_votos, $mens);
										/*								
										if (!$result_borrado_debate_votos) {
										   $mens_error.=$mens."<br/>";
										}
										else{
										   $mens_ok.="Borrados los votos de este debate <br/>";
										}*/
										
										//borramos la relacion entre votantes y modifciaciones de los administradores
										$borrado_votantes_x_admin ="DELETE FROM $tbn17 WHERE id_votante=" . $id . " ";
										$mens = "ERROR en el borrado de votantes_x_admin";
										$result_votantes_x_admin = db_query($con, $borrado_votantes_x_admin, $mens);
										//miramos que imagen tiene el usuario para borrarla
											$sql_img = "SELECT id,imagen_pequena FROM $tbn9 WHERE ID=" . $id . " ";
										    $result_img = mysqli_query($con, $sql_img);
											$row_img = mysqli_fetch_row($result_img);
											
														if ($row_img[1] != "peq_usuario.jpg") {
														 $thumb_photo_exists = $upload_user . "/" . $row_img[1];
															 if (file_exists($thumb_photo_exists)) {
																 unlink($thumb_photo_exists);
															 }					
														
											  }
										//borramos el usuario
										$borrado = "DELETE FROM $tbn9 WHERE ID=" . $id . "";
										$mens = "ERROR en el borrado del votante";
										$result = db_query($con, $borrado, $mens);
								
										if (!$result) {
										   $mens_error.=$mens."<br/>";
										}
										else{
										   $mens_ok.="Borrado el votante <br/>";
										}
										
					if ($mens_error!=""){					
                    echo "<div class=\"alert alert-danger\">".$mens_error."</div>";
					}
					if ($mens_ok!=""){
					echo "<div class=\"alert alert-success\">".$mens_ok."</div>";
					}
                    ?>
                    <p>&nbsp;</p>



                    <!--Final-->
                </div>



            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>  

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->    
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script src="../modulos/ui/jquery-ui.custom.js"></script>
        <script src="../js/jqBootstrapValidation.js"></script>
    </body>
</html>