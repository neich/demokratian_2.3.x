<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 2;
if ($nivel_acceso <= $_SESSION['usuario_nivel']) {
    header("Location: $redir?error_login=5");
    exit;
}
$fecha = date("Y-m-d H:i:s");
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><?php echo "$nombre_web"; ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content=" ">
        <meta name="author" content=" ">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png"> 



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2" >             

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-10">

                    <!--Comiezo-->
                    <h1><h1>MODIFICACIONES O BAJAS DE AFILIADOS/ SIMPATIZANTES DE FORMA MASIVA</h1></h1>
                    <?php
                    if ($inmsg != "") {
                        echo "<div class=\"alert alert-success\"> ¡¡¡Error!!!  No se han  registrado estos usuarios.</div>";
                        echo "$inmsg";
                    }
                    ?>
                    <?php echo "$mensaje1"; ?>

                    <?php
                    if ($_POST['emails'] != '') {

                      
                        $emails = strip_tags($_POST['emails'], '<\n><br/>');
                        $emailarray = explode("\n", $emails);

                        $numrows = count($emailarray);
                        for ($i = 0; $i < $numrows; $i++) {
                            $emailarray1 = explode(',', $emailarray[$i]);
                            $nif_val = fn_filtro($con, trim($emailarray1[2]));
                            $user_val = fn_filtro($con, trim($emailarray1[1]));
                            
                            if (!isset($emailarray1[0])) {
                                $mensaje1 = "Falta correo";
                             } elseif (!filter_var($emailarray1[0], FILTER_VALIDATE_EMAIL)) {
                                $error = "error";
                                $inmsg.="La direccion " . $emailarray1[0] . " es erronea. <br/>";
                            } else if ($nif_val == '') {
                                $inmsg.="ERROR , no hay nif en " . $emailarray1[0] . " <br/>";
                           
                            } else {
								
								/////miramos si el usaurio  existe	
                                $user_mail = fn_filtro($con, trim($emailarray1[0]));
                                $usuarios_consulta = mysqli_query($con, "SELECT ID FROM $tbn9 WHERE nif='" . $nif_val. "' or correo_usuario='" . $user_mail. "' ") or die(mysql_error());

                                $total_encontrados = mysqli_num_rows($usuarios_consulta);
                                $row = mysqli_fetch_row($usuarios_consulta);

                                mysqli_free_result($usuarios_consulta);

                                if ($total_encontrados != 0) {

										if ($_POST['tipo_usuario'] == 8) {
										///borramos la lista de votaciones donde ha participado
										$borrado_usuario_x_votacion ="DELETE FROM $tbn2 WHERE id_usuario=" . $row[0] . " ";
										$mens = "ERROR en el borrado de usuario_x_votacion";
										$result_usuario_x_votacion = db_query($con, $borrado_usuario_x_votacion, $mens);
										/*if (!$result_usuario_x_votacion) {
										   $mens_error.="<div class=\"alert alert-danger\">".$mens."</div>";
										}
										else{
										   $mens_ok.="Borrados la relacion de votantes de esta votación <br/>";
										}*/		
															
										//borramos debate_comentario con esta id
										$borrado_debate_comentario = "DELETE FROM $tbn12 WHERE id_votante=" . $row[0] . " ";
										$mens = "ERROR en el borrado de debate comentario";
										$result_borrado_debate_comentario = db_query($con, $borrado_debate_comentario, $mens);
								
										/*if (!$result_borrado_debate_comentario) {
										   $mens_error.="<div class=\"alert alert-danger\">".$mens."</div>";
										}
										else{
										   $mens_ok.="Borrados los comentarios de este debate <br/>";
										}*/
										
												//borramos debate_votos con esta id
										$borrado_debate_votos =  "DELETE FROM $tbn14 WHERE id_votante=" . $row[0] . "";
										$mens = "ERROR en el borrado de debate votos";
										$result_borrado_debate_votos = db_query($con, $borrado_debate_votos, $mens);
								
										/*if (!$result_borrado_debate_votos) {
										   $mens_error.="<div class=\"alert alert-danger\">".$mens."</div>";
										}
										else{
										   $mens_ok.="Borrados los votos de este debate <br/>";
										}*/
										
										
										//borramos la relacion entre votantes y modifciaciones de los administradores
										$borrado_votantes_x_admin ="DELETE FROM $tbn17 WHERE id_votante=" . $row[0] . " ";
										$mens = "ERROR en el borrado de votantes_x_admin";
										$result_votantes_x_admin = db_query($con, $borrado_votantes_x_admin, $mens);
										//miramos que imagen tiene el usuario para borrarla
											$sql_img = "SELECT id,imagen_pequena FROM $tbn9 WHERE ID=" . $val . " ";
										    $result_img = mysqli_query($con, $sql_img);
											$row_img = mysqli_fetch_row($result_img);
											
														if ($row_img[1] != "peq_usuario.jpg") {
														 $thumb_photo_exists = $upload_user . "/" . $row_img[1];
															 if (file_exists($thumb_photo_exists)) {
																 unlink($thumb_photo_exists);
															 }					
														
											  }
												
										// y por ultimo borramos el votante	
                                        $sSQL = "DELETE FROM $tbn9 WHERE id=" . $row[0] . "";
                                        $mens="Error al eliminar los  datos de votantes";
                                        $result = db_query($con, $sSQL, $mens);
                                            if (!$result) {
                                                 echo "<div class=\"alert alert-danger\">
                                                 <strong> UPSSS!!!!<br/>esto es embarazoso, hay un error en la conexion con la base de datos </strong> </div><strong>";
                                             }
                                            if ($result) {
                                        $mensaje1.=" -> Usuario: " . trim($emailarray1[1]) . " . Correo: " . trim($emailarray1[0]) . " NIF: " . trim($emailarray1[2]) . " ¡¡¡ELIMINADO!!!<br/>";
                                         											$fecha = date("Y-m-d H:i:s");
									        $accion = "3"; //borrar votante
											$insql = "insert into $tbn17 (id_votante,id_admin,accion,fecha ) values (  \"" . $row[0] . "\",\"" . $_SESSION['ID'] . "\",   \"$accion\", \"$fecha\" )";
                                            $mens="error incluir quien ha borrado el votantes";											
											$resultado = db_query($con, $insql, $mens);  
										   
										    }
                                        // borrado
                                    } else {  ///modificamos
                                        $tipo_usuario=fn_filtro($con,$_POST['tipo_usuario']);
                                        $sSQL = "UPDATE $tbn9 SET   tipo_votante=" . $tipo_usuario . "  WHERE id=" . $row[0] . "";
                                        $mens="Error al modificar datos de votantes";
                                        $result = db_query($con, $sSQL, $mens);
                                        //mysqli_query($con, $sSQL) or die("Imposible modificar pagina");
                                            if (!$result) {
                                                 echo "<div class=\"alert alert-danger\">
                                                 <strong> UPSSS!!!!<br/>esto es embarazoso, hay un error en la conexion con la base de datos </strong> </div><strong>";
                                             }
                                            if ($result) {

                                                $mensaje1.=" -> Usuario: " . trim($emailarray1[1]) . " . Correo: " . trim($emailarray1[0]) . " NIF: " . trim($emailarray1[2]) . " Convertido en usario tipo --> " . $tipo_usuario . "<br/>";
                                           		
												$accion = "2"; //modificar votante
												$insql = "insert into $tbn17 (id_votante,id_admin,accion,fecha ) values (  \"" . $row[0] . "\",\"" . $_SESSION['ID'] . "\",   \"$accion\", \"$fecha\" )";
												$mens="error incluir quien ha borrado el votantes";											
												$resultado = db_query($con, $insql, $mens); 
										    }
                                    }
                                } else {

                                    $inmsg.= " El Usuario " . trim($emailarray1[1]) . " con correo " . trim($emailarray1[0]) . "  y nif " . trim($emailarray1[2]) . " NO está registrado.";
                                }
                            }

                            //$mensaje1a.="<br /><p>Estos usuarios han sido modificados:</p><br/>";
                        }
                    }
                    ?>

                    Para dar de bajas asuscriptores a su lista escriba  la dirección de correo, el nombre y  el nif separadas por una coma (,) Use una linea para cada suscriptor , si desconoce el nombre ponga algo como, miembro, usuario..etc  ya que este dato no se puede quedar vacio. La direccion de correo es imprescindible.<br />
                    <br/>
                    Ejemplo: <br/>
                    <ul>
                        carlos@yahoo.es, carlos, 384955l <br/>
                        juan@iespana.es, juan erez, 384752z <br/>
                        etc
                    </ul>
                    <?php if ($inmsg != "") { ?> <br/><div class="alert alert-warning"> <?php echo "$inmsg"; ?> </div> <?php } ?>

                    <?php if ($mensaje1 != "") { ?>
                        <br/><div class="alert alert-success"> Estos usuarios han sido moificados:</div>
                        <div class="alert alert-success">         
                            <?php echo "$mensaje1"; ?></div><?php } ?>

                    <?php if ($mensaje1a != "") { ?>
                        <br/><div class="alert alert-success"> Estos usuarios han sido añadidos:</div>
                        <div class="alert alert-success">         
                            <?php echo "$mensaje1a"; ?></div><?php } ?>

                    <form Action="<?php $_SERVER['PHP_SELF'] ?>" Method=POST class="well form-horizontal">	


                        <p>¿Que dessea hacer?, dar de baja o convertir en simpatizante si son afiliados</p>
                        <p>Convertir en </p>
                        <table width="100%">
                            <tr>
                                <td width="15%" height="29">
                                    <input name="tipo_usuario" type="radio" id="tipo_usuario_0" value="1" /></td>
                                <td width="15%"><input name="tipo_usuario" type="radio" id="tipo_usuario_1" value="2" checked="checked" />
                                    (2)</td>
                                <td width="15%"><input name="tipo_usuario" type="radio" id="tipo_usuario_3" value="3" />
                                    (3)</td>
                                <td width="15%"><input name="tipo_usuario" type="radio" id="tipo_usuario_2" value="9" /></td>
                                <td width="40%"><input name="tipo_usuario" type="radio" id="tipo_usuario_4" value="8" /></td>
                            </tr>
                            <tr>
                                <td height="22">afiliado</td>
                                <td>simpatizante verificado </td>
                                <td> simpatizantes </td>
                                <td>Dar de Baja</td>
                                <td>Borrar (No se recomienda en absoluto, Puede dar problemas en las votaciones en las que ha participado, si no esta seguro, use dar de baja)</td>
                            </tr>
                        </table>
                        <p>
                          <label for="emails"></label>
                            <textarea name="emails" cols="80" rows="30" id="emails" class="form-control" ></textarea>
                        </p>
                        <p>&nbsp;</p>   <input type="submit" value="Modificar/borrar votantes de la lista"  class="btn btn-primary pull-right" > <p>&nbsp;</p>

                    </form>





                    <!--Final-->
                </div>



            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>  

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->    
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>

    </body>
</html>