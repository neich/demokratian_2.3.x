<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 11;
if ($nivel_acceso <= $_SESSION['usuario_nivel']) {
    header("Location: $redir?error_login=5");
    exit;
}
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><?php echo "$nombre_web"; ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content=" ">
        <meta name="author" content=" ">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png"> 



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <!-- NAVBAR
      ================================================== -->
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2" >             

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-10">

                    <!--Comiezo-->

                    <h1>&nbsp;</h1>
                    <h1>BUSCAR  EN EL CENSO DE VOTANTES</h1>
                    <p>&nbsp;</p>
                    <!---->

                    <form id="form1" name="form1" method="post" action="bloquear_censos_busq.php" class="well form-horizontal">

                        <div class="form-group">       
                            <label for="nombre" class="col-sm-3 control-label">Estado</label>

                            <div class="col-sm-9">

                                <label>
                                    <input name="bloqueado" type="radio" id="bloqueado_0" value="si" checked="checked" />
                                    bloqueado</label>
                                <br />
                                <label>
                                    <input type="radio" name="bloqueado" value="no" id="bloqueado_1" />
                                    desbloqueado</label>

                            </div></div>
                        <div class="form-group">       
                            <label for="nombre" class="col-sm-3 control-label">Nombre </label>

                            <div class="col-sm-9">
                                <input type="text" name="nombre_usuario" id="nombre_usuario" class="form-control"/>

                            </div></div>
                        <div class="form-group">       
                            <label for="nombre" class="col-sm-3 control-label">Correo electronico  </label>

                            <div class="col-sm-9">
                                <input type="text" name="correo_electronico" id="correo_electronico" class="form-control"/>
                            </div></div>
                        <div class="form-group">       
                            <label for="nombre" class="col-sm-3 control-label">Nif</label>

                            <div class="col-sm-4">

                                <input type="text" name="nif" id="nif" class="form-control"/>
                            </div></div>
                        <div class="form-group">       
                            <label for="nombre" class="col-sm-3 control-label"> Tipo usuario </label>

                            <div class="col-sm-9">

                                <input name="tipo_usuario" type="radio" id="tipo_usuario_3" checked="checked" /> 
                                todos <br/>

                                <input name="tipo_usuario" type="radio" id="tipo_usuario_0" value="1" />
                                solo socios (1) <br/>


                                <input type="radio" name="tipo_usuario" value="2" id="tipo_usuario_1" />
                                socios y simpatizantes verificados (2) <br/>

                                <input type="radio" name="tipo_usuario" value="3" id="tipo_usuario_4" />
                                socios y simpatizantes (3)<br/>

                                <input type="radio" name="tipo_usuario" value="4" id="tipo_usuario_2" />
                                Abierta (3) 

                            </div></div>
                       <?php if($es_municipal==false){?>
                        <div class="form-group">       
                            <label for="nombre" class="col-sm-3 control-label">Provincia</label>

                            <div class="col-sm-9">

                                <?php
                                if ($_SESSION['nivel_usu'] == 2) {
                                    // listar para meter en una lista del cuestionario buscador


                                    $options = "select DISTINCT id, provincia from $tbn8  where especial=0 order by ID";
                                    $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());

                                    while ($listrows = mysqli_fetch_array($resulta)) {
                                        $id_pro = $listrows[id];
                                        $name1 = $listrows[provincia];
                                        if ($id_pro == $row[1]) {
                                            $check = "selected=\"selected\" ";
                                        } else {
                                            $check = "";
                                        }
                                        $lista1 .="<option value=\"$id_pro\"  $check> $name1</option>";
                                    }
                                    ?>
                                    <h3> Escoja una Provincia </h3>
                                    <select name="provincia" class="form-control" id="provincia" >
                                        <?php echo "$lista1"; ?>
                                    </select>
                                    <?php
                                } else if ($_SESSION['nivel_usu'] == 3) {

                                    $options = "select DISTINCT id, provincia from $tbn8  where id_ccaa=" . $_SESSION['id_ccaa_usu'] . "  order by ID";
                                    $resulta = mysqli_query($con, $options) or die("error: " . mysqli_error());

                                    while ($listrows = mysqli_fetch_array($resulta)) {
                                        $id_pro = $listrows[id];
                                        $name1 = $listrows[provincia];
                                        if ($id_pro == $row[1]) {
                                            $check = "selected=\"selected\" ";
                                        } else {
                                            $check = "";
                                        }
                                        $lista1 .="<option value=\"$id_pro\"  $check> $name1</option>";
                                    }
                                    ?>
                                    <h3> Escoja una Provincia </h3>
                                    <select name="provincia" class="form-control" id="provincia" >
                                        <?php echo "$lista1"; ?>
                                    </select>
                                    <?php
                                } else {

                                    $result2 = mysqli_query($con, "SELECT id_provincia FROM $tbn5 where id_usuario=" . $_SESSION['ID']);
                                    $quants2 = mysqli_num_rows($result2);
//$row2=mysqli_fetch_row($result2);

                                    if ($quants2 != 0) {

                                        while ($listrows2 = mysqli_fetch_array($result2)) {

                                            $name2 = $listrows2[id_provincia];
                                            $optiones = mysqli_query($con, "SELECT  provincia FROM $tbn8 where ID=$name2");
                                            $row_prov = mysqli_fetch_row($optiones);
                                            if ($acc == "modifika") {
                                                if ($name2 == $row[1]) {
                                                    $check = "checked=\"checked\" ";
                                                } else {
                                                    $check = "";
                                                }
                                            } else {
                                                $check = "checked=\"checked\" ";
                                            }

                                            $lista1 .="    <label><input  type=\"radio\" name=\"provincia\" value=\"$name2\"   $check id=\"provincia\" /> " . $row_prov[0] . "</label> <br/>";
                                        }
                                        echo "$lista1";
                                    } else {
                                        echo " No tiene asignadas provincias, no podra crear votación";
                                    }
                                }
                                ?>
                            </div></div>
                       <?php } ?>
                        <!--Final-->
                        <input type="submit" name="buscar" id="buscar" value="Buscar"  class="btn btn-primary pull-right"  />
                        <br/><br/>
                    </form>


                </div>



            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>  

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->    
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>

    </body>
</html>