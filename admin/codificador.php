<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
include('../inc_web/seguri_nivel.php');
$nivel_acceso = 7;
if ($nivel_acceso <= $_SESSION['usuario_nivel']) {
    header("Location: $redir?error_login=5");
    exit;
}

$idvot = fn_filtro_numerico($con, $_GET['idvot']);
$fecha = date("Y-m-d H:i:s");

if ($_GET['id'] != "") {
    $id = fn_filtro_numerico($con, $_GET['id']);
    $acc = fn_filtro_nodb($_GET['acc']);
}



if (ISSET($_POST["modifika_codificador"])) {

    $nombre = fn_filtro($con, $_POST['nombre']);
    $apellido = fn_filtro($con, $_POST['apellido']);
    $orden = fn_filtro($con, $_POST['orden']);

    $mail_expr = "/^[0-9a-z~!#$%&_-]([.]?[0-9a-z~!#$%&_-])*"
            . "@[0-9a-z~!#$%&_-]([.]?[0-9a-z~!#$%&_-])*$/";

    if (empty($_POST['correo'])) {
        $texto1 = "<div class=\"alert alert-danger\">¡¡¡ERROR!!! La direccion de correo es un dato necesario</div>";
    } elseif (!preg_match($mail_expr, $_POST['correo'])) {

        $texto1 = "<div class=\"alert alert-danger\">¡¡¡ERROR!!! <br/>la direccion es erronea</div>";
    } else {
        $correo = fn_filtro($con, $_POST['correo']);
        $nombre_usuario = $_SESSION['nombre_usu'];

        $sSQL = "UPDATE $tbn21 SET nombre=\"$nombre\",correo=\"$correo\" ,fecha_modif=\"$fecha\",  modif=\"$nombre_usuario\" ,  orden=\"$orden\"  WHERE id='$id'";

        mysqli_query($con, $sSQL) or die("Imposible modificar pagina");

        $texto1 = "<div class=\"alert alert-success\">Realizadas las Modificaciones <br>Asi ha quedado el codificador $nombre  </div>";
    }
}

if (ISSET($_POST["add_codificador"])) {

    $nombre = fn_filtro($con, $_POST['nombre']);
    $id_votacion = fn_filtro_numerico($con, $_POST['id_vot']);
    $orden = fn_filtro_numerico($con, $_POST['orden']);

    $mail_expr = "/^[0-9a-z~!#$%&_-]([.]?[0-9a-z~!#$%&_-])*"
            . "@[0-9a-z~!#$%&_-]([.]?[0-9a-z~!#$%&_-])*$/";

    if (empty($_POST['correo'])) {
        $texto1 = "<div class=\"alert alert-danger\">¡¡¡ERROR!!! La direccion de correo es un dato necesario</div>";
    } elseif (!preg_match($mail_expr, $_POST['correo'])) {

        $texto1 = "<div class=\"alert alert-danger\">¡¡¡ERROR!!! <br/>la direccion es erronea</div>";
    } else {
        $correo = fn_filtro($con, $_POST['correo']);
        $usuario = $_SESSION['ID'];
        $nivel_acceso = 0;
        $insql = "insert into $tbn21 (nombre,  correo,id_votacion,anadido, fecha_anadido,nivel_acceso,orden) values (  \"$nombre\",  \"$correo\", \"$id_votacion\", \"$usuario\", \"$fecha\", \"$nivel_acceso\", \"$orden\")";
        $inres = @mysqli_query($con, $insql) or die("<strong><font color=#FF0000 size=3>  Imposible añadir. Cambie los datos e intentelo de nuevo.</font></strong>");
        $texto1 = "<div class=\"alert alert-success\">Añadido codificador <br/> $nombre con correo  $correo <br/> a la base de datos</div> ";
    }
}
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><?php echo "$nombre_web"; ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content=" ">
        <meta name="author" content=" ">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png"> 



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>
        <p><!-- NAVBAR
      ================================================== -->
        </p>
        <?php include("../admin/menu_admin.php"); ?>

        <!-- END NAVBAR
        ================================================== -->

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->
            <?php include("../votacion/caja_mensajes_1.php"); ?>

            <div class="row">




                <div class="col-md-2" >             

                    <?php include("../votacion/menu_nav.php"); ?>

                </div>



                <div class="col-md-10">

                    <!--Comiezo-->
                    <?php
                    $result_vot = mysqli_query($con, "SELECT nombre_votacion,seguridad,interventor, interventores FROM $tbn1 where id=$idvot");
                    $row_vot = mysqli_fetch_row($result_vot);
                    ?>

                    <h1> <?php if ($acc == "modifika") { ?>
                            <?php
                            $result = mysqli_query($con, "SELECT orden,nombre,correo FROM $tbn21 where id=$id");
                            $row = mysqli_fetch_row($result);
                            $orden = $row[0];
                            ?>
                            MODIFICAR codificador
                        <?php } else { ?>
                            INCLUIR codificador <?php } ?></h1>
                    <h3>   en la votación   <?php echo $row_vot[0]; ?></h3>
                    <div class="alert alert-danger">OJO, recuerde que cuantos más codificadores tenga, mas se ralentiza el proceso de votación y el servidor puede tener problemas de capacidad. No se recomienda más de 2 por votación.</div>
                    <p><?php echo"$texto1"; ?></p>
                    <p><a href="codificador_busq1.php?idvot=<?php echo $idvot; ?>">Ver codificadores de esta votación </a></p>
                    <form action="<?php $_SERVER['PHP_SELF'] ?>" method=post   name="frmDatos" id="frmDatos"  class="well form-horizontal" >

                        <div class="form-group">       
                            <label for="nombre" class="col-sm-3 control-label">Nombre y apellidos</label>

                            <div class="col-sm-9">   

                                <input name="nombre" type="text"  id="nombre" value="<?php echo "$row[1]"; ?>"   class="form-control" placeholder="Nombre" required autofocus data-validation-required-message="El nombre  es un dato requerido" />
                            </div></div>

                        <div class="form-group">       
                            <label for="nombre" class="col-sm-3 control-label">Correo</label>

                            <div class="col-sm-9">   
                                <input name="correo" type="email"  id="correo" value="<?php echo "$row[2]"; ?>"   class="form-control" placeholder="Correo electronico" required  data-validation-required-message="Por favor, ponga un correo electronico"  />
                            </div></div>

                        <div class="form-group">       
                            <label for="nombre" class="col-sm-3 control-label">Orden</label>

                            <div class="col-sm-2">  
                                <?php
                                if ($acc == "") {

                                    $result = mysqli_query($con, "SELECT orden FROM $tbn21 where id_votacion=$idvot order by orden DESC ");
                                    $row = mysqli_fetch_row($result);
                                    $orden = $row[0] + 1;
                                }
                                ?>

                                <input name="orden" type="number" class="form-control" id="orden" value="<?php echo "$orden"; ?>" min="0" required />
                            </div>
                            <div class="col-sm-7"> 
                                <p>Ojo, es importante que no existan numeros duplicados, si es asi tendra errores</p>
                            </div>
                        </div>




                        <input name="incluido" type="hidden" id="incluido" value="<?php echo $_SESSION['ID']; ?>" />
                        <input name="id_vot" type="hidden" id="id_vot" value="<?php echo"$idvot"; ?>">
                        <input name="fecha" type="hidden" id="fecha" value="<?php echo"$fecha"; ?>" />

                        <?php if ($acc == "modifika") { ?>
                            <input name="modifika_codificador" type=submit  class="btn btn-primary pull-right"  id="modifika_codificador" value="ACTUALIZAR  codificador" />
                        <?php } else { ?>
                            <input name="add_codificador" type=submit class="btn btn-primary pull-right"  id="add_codificador" value="AÑADIR codificador" />
                        <?php } ?>
                        <p>&nbsp;</p>
                    </form>




                    <!--Final-->
                </div>



            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->
                <?php include("../votacion/ayuda.php"); ?>
                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>  

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->    
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>

    </body>
</html>