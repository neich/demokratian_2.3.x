<?php
###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
 include('../inc_web/seguri_nivel.php');
$nivel_acceso = 0;
if ($nivel_acceso = $_SESSION['usuario_nivel']) {
    header("Location: $redir?error_login=5");
    exit;
}

require_once('../modulos/PHPMailer/class.phpmailer.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>Ayuda</title>  
    </head>
    <body>

        <div class="modal-content">
            <div class="modal-header">
                <a class="close" data-dismiss="modal" >x</a>
                <!--    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                <h4 class="modal-title">Prueba de configuracón de correo</h4>

            </div>            <!-- /modal-header -->
            <div class="modal-body">
                <p>
                <?php

					$name="prueba correo";
					$mensaje="Es una prueba de configuracion de correo";
					
					
					$mail = new PHPMailer;
					
					//Enable SMTP debugging. 
					$mail->SMTPDebug = 2;                               
					//Set PHPMailer to use SMTP.
							if ($mail_IsHTML == true) {
								$mail->IsHTML(true);
							} else {
								$mail->IsHTML(false);
							}
					
							if ($mail_sendmail == true) {
								$mail->IsSendMail();
							} else {
								$mail->IsSMTP();
							}            
							//$mail->SMTPAuth = true;
							if ($mail_SMTPAuth == true) {
								$mail->SMTPAuth = true;
							} else {
								$mail->SMTPAuth = false;
							}
							
							if ($mail_SMTPSecure == false) {
								
							}else if ($mail_SMTPSecure == "SSL") {
								$mail->SMTPSecure = 'ssl';
							} else {
								$mail->SMTPSecure = 'tls';
							}	
							$mail->Port = $puerto_mail; // Puerto a utilizar, normalmente es el 25   
								   
							$mail->Host = $host_smtp;
							$mail->SetFrom($email_env, $nombre_sistema);
							$mail->MsgHTML($mensaje);
							$mail->AddAddress($email_env, $name);
							$mail->Username = $user_mail;
							$mail->Password = $pass_mail;
					
							$mail->Subject = "prueba de configuracion";
							$mail->Body = "<i>Texto de correo en HTML</i>";
							$mail->AltBody = "This is the plain text version of the email content";
							
						if(!$mail->send()) 
							{
								echo "Mailer Error: " . $mail->ErrorInfo;
							} 
						else 
							{
								echo "<br/>El correo ha sido enviado, la configuración es correcta <br/>";
							}
					?>
                
                
                </p>



                <!--
            ===========================  fin texto ayuda
                -->             </div>            <!-- /modal-body -->
            <!-- /modal-footer -->
        </div>         <!-- /modal-content -->

    </body>
</html>
