<?php

###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
require_once('../modulos/PHPMailer/class.phpmailer.php');

//require_once("../modulos/PHPMailer/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded
######################### Funciones de seguridad para evitar ataques por XSS #########################
function fn_filtro_editor($conexion, $cadena) {
    if (get_magic_quotes_gpc() != 0) {
        $cadena = stripslashes($cadena);
    }
    
    $vowels = array("javascript", "alert(", "alert (", "cookie", "onload", "document.location", "onclick", "onmouseover", "onmouseout", "onunload", "onsubmit", "onselect", "onresize", "onreset", "onmouseup", "onmousemove", "onmousedown", "onkeyup", "onkeypress", "onkeydown", "onfocus", "ondblclick", "onresize", "onclick", "onblur", "onchange", "getElementById", "this");
    $cadena1 = str_ireplace($vowels, "****", $cadena);
    $cadena2 = strip_tags($cadena1, '<h1><h2><h3><h4><h5><h6><p><hr><pre><blockquote><ol><ul><li><dl><dt>< dd><div><center><a><basefont><br><em><font><span><strong><table><caption><colgroup><col><tbody><thead><tfoot><tr><td><th><img>');
    return mysqli_real_escape_string($conexion, $cadena2);
}

function fn_filtro($conexion, $cadena) {
    if (get_magic_quotes_gpc() != 0) {
        $cadena = stripslashes($cadena);
    }
    $cadena = strip_tags($cadena);
    return mysqli_real_escape_string($conexion, $cadena);
}

function fn_filtro_numerico($conexion, $cadena) {
    if (get_magic_quotes_gpc() != 0) {
        $cadena = stripslashes($cadena);
    }

    if (is_numeric($cadena)) {
        $cadena = $cadena;
    } else {
        $cadena = "";
        Header("Location: ../index.php?error_login=4");
        exit;
    }
    $cadena = strip_tags($cadena);
    return mysqli_real_escape_string($conexion, $cadena);
}

function fn_filtro_nodb($cadena) {
    if (get_magic_quotes_gpc() != 0) {
        $cadena = stripslashes($cadena);
    }

    return strip_tags($cadena);
}

function fn_filtro_nodb_numerico($cadena) {
    if (get_magic_quotes_gpc() != 0) {
        $cadena = stripslashes($cadena);
    }
    if (is_numeric($cadena)) {
        $cadena = $cadena;
    } else {
        $cadena = "";
        Header("Location: ../index.php?error_login=4");
        exit;
    }
    return strip_tags($cadena);
}

###################### FIN de Funciones de seguridad para evitar ataques por XSS #########################
####################################  Funciones de control de errores de inserccion a la BBDD  ##########################################
//$b_debugmode = 1; // 0 || 1

function db_query($conexion, $query, $mensaje_esp) {
    global $b_debugmode;

    // Perform Query
    $result = mysqli_query($conexion, $query);

    // Check result
    // This shows the actual query sent to MySQL, and the error. Useful for debugging.
    if (!$result) {

        $message = '<b>Invalid query:</b><br>' . mysqli_error($conexion) . '<br><br>';
        $message .= '<b>Whole query:</b><br>' . $query . '<br><br>';
        $message .= "\r\n " . $mensaje_esp;

        if ($b_debugmode) {

            die($message);
        }

        raise_error('db_query_error: ' . $message);
    }

    return $result;
}

####################################  Funcion de control de errores de insercion a la BBDD que devuelve el id del ultimo registro ##########################################
//$b_debugmode = 1; // 0 || 1

function db_query_id($conexion, $query, $mensaje_esp) {
    global $b_debugmode;

    // Perform Query
    $result = mysqli_query($conexion, $query);

    // Check result
    // This shows the actual query sent to MySQL, and the error. Useful for debugging.
    if (!$result) {

        $message = '<b>Invalid query:</b><br>' . mysqli_error($conexion) . '<br><br>';
        $message .= '<b>Whole query:</b><br>' . $query . '<br><br>';
        $message .= "\r\n " . $mensaje_esp;

        if ($b_debugmode) {

            die($message);
        }

        raise_error('db_query_error: ' . $message);
    }

    return mysqli_insert_id($conexion);
}

############################### FUNCION DE ENVIO DE MESAJE SI HAY ERROR Y AÑADIR A LOGS #######################3

function raise_error($message) {
    global $email_error_tecnico, $email_sistema, $correo_smtp, $user_mail, $pass_mail, $mail_IsHTML, $mail_sendmail, $mail_SMTPAuth, $mail_SMTPSecure, $puerto_mail;

    $serror = "Env:       " . $_SERVER['SERVER_NAME'] . "\r\n" .
            "timestamp: " . date('m/d/Y H:i:s') . "\r\n" .
            "script:    " . $_SERVER['PHP_SELF'] . "\r\n" .
            "error:     " . $message . "\r\n\r\n";

    // ABRIR EL FICHERO DE LOGS Y ESCRIBIR EL ERROR
    $fhandle = fopen('../logs/errors' . date('Ymd') . '.txt', 'a');
    if ($fhandle) {
        fwrite($fhandle, $serror);
        fclose(( $fhandle));
    }

    // ENVIAR UN CORREO ELECTRONICO
    if (!$b_debugmode) {
        //mail($email_error_tecnico, 'error: '.$message, $serror, 'From: ' . $email_sistema );
        $asunto_mens_error = "ERROR SISTEMA VOTACIONES | problema base de datos";
        $nombre_ref = "Sistema";
        $nombre_eq = "tecnico";

        if ($correo_smtp == true) {  //comienzo envio smtp
            $mail = new PHPMailer();
            //$mail->SMTPDebug = 2;////habilitamos los errores de envios  para saber que pasa
            $mail->CharSet = 'UTF-8';
            $mail->ContentType = 'text/plain';
            if ($mail_IsHTML == true) {
                $mail->IsHTML(true);
            } else {
                $mail->IsHTML(false);
            }

            if ($mail_sendmail == true) {
                $mail->IsSendMail();
            } else {
                $mail->IsSMTP();
            }

            //$mail->SMTPAuth = true;
            if ($mail_SMTPAuth == true) {
                $mail->SMTPAuth = true;
            } else {
                $mail->SMTPAuth = false;
            }

            if ($mail_SMTPSecure == false) {
                
            } else if ($mail_SMTPSecure == "SSL") {
                $mail->SMTPSecure = 'ssl';
            } else {
                $mail->SMTPSecure = 'tls';
            }

            $mail->Port = $puerto_mail; // Puerto a utilizar, normalmente es el 25   
            $mail->Host = $host_smtp;
            $mail->SetFrom($email_sistema, $nombre_ref);
            $mail->Subject = $asunto_mens_error;

            $mail->MsgHTML($serror);

            $mail->AddAddress($email_error_tecnico, $nombre_eq);


            $mail->Username = $user_mail;
            $mail->Password = $pass_mail;

            if (!$mail->Send()) {
                echo " Error en el envio " . $mail->ErrorInfo;
            } else {
                // echo "Enviado correctamente!";
            }
        }// fin envio por stmp

        if ($correo_smtp == false) { ///correo mediante mail de php
            //para el envío en formato HTML 
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

            //dirección del remitente 
            $headers .= "From: $nombre_ref <$email_sistema>\r\n";
            //ruta del mensaje desde origen a destino 
            $headers .= "Return-path: $email_sistema\r\n";

            $asunto = "$asunto_mens_error";
            mail($email_error_tecnico, $asunto_mens_error, $serror, $headers);
        }
    }
}

##############################  FIN  de  Funciones de control de errores de inserccion a la BBDD  ##########################################
?>