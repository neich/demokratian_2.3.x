<?php 

/*	 // Idioma
	$lang = "ca_ES";
	
	// Dominio
	$nombre_lang="UserLang_ca_ES";
	
	// Dependiendo de tu OS putenv/setlocale configurarán tu idioma.
	putenv('LC_ALL='.$lang);
	setlocale(LC_ALL, $lang);
	//miramos si existe la funcion gettest
	if (false === function_exists('gettext'))
		{
			echo "No tienes la libreria gettext instalada.";
			exit(1);
		}
	// La ruta a los archivos de traducción
	bindtextdomain($nombre_lang, '../locale' );
	
	// El codeset del textdomain
	bind_textdomain_codeset($nombre_lang, 'UTF-8');
	
	// El Textdomain
	textdomain($nombre_lang);
*/
/* if (false === function_exists('gettext'))
		{
			echo "No tienes la libreria gettext instalada.";
			exit(1);
		}
	// Idioma
	
		$lang = "ca_ES";
		
	//Codifciación--> ver si es mejor usar bind_textdomain_codeset($nombre_lang, 'UTF-8');
		$lang_cod = "ca_ES.utf8";
	// Nombre del archivo de la traduccion
	$nombre_lang="UserLang_ca_ES";
	
	setlocale(LC_ALL, 'ca_ES','ca_ES.utf8');
	bindtextdomain('UserLang_ca_ES','./locale/');
	textdomain('UserLang_ca_ES'); */
	
   /*@bindtextdomain('default', dirname(__FILE__).'/');	 	 
   @textdomain ('default');	 	 
   $langs = array (	 	 
    'es' => 'ES',	 	 
    'en' => 'GB',	 	 
   );	 	 
   $code = isset($_REQUEST['lang'])?$_REQUEST['lang']:'es';	 	 
   if (isset($langs[$code]))	 	 
     $iso_code = $code.'_'.$langs[$code];	 	 
   else{	 	 
     $code = "es";	 	 
     $iso_code = 'es_ES';	 	 
   }	 	 
   if (isset($_SESSION['lang'])) $_SESSION['lang']=$code;	 	 
   putenv ('LANGUAGE='.$iso_code);	 	 
   putenv ("LC_ALL=$iso_code");	 	
   
   ///
   if(!function_exists('_')){
    function _($key){ return $key; }
}
*/
/*$directory = dirname(__FILE__).'/locale';
$domain = 'UserLang_ca_ES';
$locale ="ca_ES";

//putenv("LANG=".$locale); //not needed for my tests, but people say it's useful for windows

setlocale( LC_ALL, $locale);
bindtextdomain($domain, $directory);
textdomain($domain);
bind_textdomain_codeset($domain, 'UTF-8');
*/

/*$language = "ca_ES";

if (defined('LC_MESSAGES')) {
    setlocale(LC_MESSAGES, $language); // Linux
	echo " texto";
} else {
    putenv("LC_ALL={$language}"); // windows
	
}

putenv("LC_ALL=$language");
setlocale(LC_ALL, $language);
bindtextdomain("UserLang_ca_ES", "./locale");
textdomain("UserLang_ca_ES");
*/

// Configurar idioma en Alemán
putenv('LC_ALL=ca_ES');
setlocale(LC_ALL, 'ca_ES');

// Especifica la ubicación de la tabla de traducciones
bindtextdomain("UserLang_ca_ES", "./locale");

// Seleccionar dominio
textdomain("UserLang_ca_ES");

// Ahora las traducciones se encuentran en ./locale/de_DE/LC_MESSAGES/myPHPApp.mo

?>