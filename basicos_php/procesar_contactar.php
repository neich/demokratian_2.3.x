<?php

###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
require_once("../config/config.inc.php");
require_once("../inc_web/conexion.php");

require_once('../modulos/PHPMailer/class.phpmailer.php');
include("../modulos/PHPMailer/class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded
include("../basicos_php/basico.php");


if (empty($_POST['name']) ||
        empty($_POST['email']) ||
        empty($_POST['texto']) ||
        !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    echo "ERROR##<div class=\"alert alert-danger\">No se han enviado argmentos!</div>";
    return false;
}

$name = fn_filtro_nodb($_POST['name']);
$email = fn_filtro_nodb($_POST['email']);

$texto = fn_filtro_nodb($_POST['texto']);

$nombre_cod = utf8_decode($name);


if($es_municipal==false){
		$provincia = fn_filtro_numerico($con, $_POST['provincia']);	
		///miramos la direccion de correo a la que hay que enviar el correo segun la provincia
		
		$options2 = "select provincia,  correo_notificaciones from $tbn8 where id ='$provincia' ";
		$resulta2 = mysqli_query($con, $options2) or die("error: " . mysqli_error());
		
		//$nombre_provincia= mysqli_result($resulta2,0,'provincia'); 
		$linea = mysqli_fetch_row($resulta2);
		$nombre_provincia = $linea[0];
		
		//if(mysqli_result($resulta2,0,'correo_notificaciones')!=""){
		if ($linea[1] != "") {
			//$correo_error= mysqli_result($resulta2,0,'correo_notificaciones'); 
			$correo_error = $linea[1];
		} else {
			$correo_error = $email_error; //si no hay resultados metemos el de configuracion general
		}

}else{
$correo_error = $email_error; //si no hay resultados metemos el de configuracion general

}


$mensaje = "Hola \r\n";

$mensaje .= "<strong>" . $name . " </strong> Se ha puesto en contacto mediante el formulario de correo electonico de " . $nombre_web . " \n";

$mensaje .= "El " . date('d/m/Y', time());

$mensaje .="\r\n
con correo electronico " . $email . " \n ";
if($es_municipal==false){
$mensaje .="Y pertenece a la provincia " . $provincia . " \r\n";
}
$mensaje .="y envia el siguiente texto: \r\n " . $texto . " ";



$mensaje = str_replace("\n", "<br>", $mensaje);
$mensaje = str_replace("\t", "    ", $mensaje);

if ($correo_smtp == true) {  //comienzo envio smtp
    $asunto_ = "Contacto registro | " . $nombre_web . "";
	
	$mail = new PHPMailer();
    $mail->CharSet = 'UTF-8';
    $mail->ContentType = 'text/html';
	
	//$mail->IsHTML(false);
    if ($mail_IsHTML == true) {
        $mail->IsHTML(true);
    } else {
        $mail->IsHTML(false);
    }

    if ($mail_sendmail == true) {
        $mail->IsSendMail();
    } else {
        $mail->IsSMTP();
    }
	
    //$mail->SMTPAuth = true;
     if ($mail_SMTPAuth == true) {
         $mail->SMTPAuth = true;
     } else {
         $mail->SMTPAuth = false;
     }  

	if ($mail_SMTPSecure == false) {
			
	}else if ($mail_SMTPSecure == "SSL") {
        $mail->SMTPSecure = 'ssl';
    } else {
        $mail->SMTPSecure = 'tls';
    }				 
	 
    $mail->Port = $puerto_mail; // Puerto a utilizar, normalmente es el 25	

    $mail->Host = $host_smtp;
    $mail->SetFrom($email, $nombre_cod);
    $mail->Subject = $asunto_;
    $mail->MsgHTML($mensaje);
    $mail->AddAddress($correo_error, $nombre_web);    
    $mail->Username = $user_mail;
    $mail->Password = $pass_mail;


    if (!$mail->Send()) {
        echo "<div class=\"alert alert-danger\"> ERROR## Error en el envio " . $mail->ErrorInfo."</div>";
    } else {
        // echo "Enviado correctamente!";

        echo " 
OK##<div class=\"alert alert-success\">
    <strong>Se ha enviado su mensaje </strong><br/> 
	En breve le contestaremos <br/>
	Muchas gracias por contactar con nosotros
	</div>";
    }
}


if ($correo_smtp == false) { ///correo mediante mail de php
    //para el envío en formato HTML 
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

//dirección del remitente 
    $headers .= "From: $nombre_cod<$email>\r\n";


//ruta del mensaje desde origen a destino 
    $headers .= "Return-path: $email\r\n";

//$asunto="$asunto_mens_error";


    mail($correo_error, $asunto_, $mensaje, $headers);

    echo "<div class=\"alert alert-success\">OK##enviado correo por mail</div>";
}




return true;
?>