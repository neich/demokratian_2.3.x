<?php 
function send_smtp_mail($to_name, $to, $subject, $body, $from_name, $from, $reply_to_name, $reply_to)
{
     $mail = new PHPMailer(true);  // the true param means it will throw exceptions on errors, which we need to catch
     $mail->IsSMTP();  // telling the class to use SMTP
     try{
          $mail->Host       = "smtp.googlemail.com";  // SMTP server 
          $mail->Port       = 465;                    // SMTP port 
          $mail->Username   = "username@yourdomain";  // GMAIL username
          $mail->Password   = "password";             // GMAIL password
          $mail->SMTPDebug  = 0;        // 1 to enables SMTP debug (for testing), 0 to disable debug (for production)
          $mail->SMTPAuth   = true;    // enable SMTP authentication
          $mail->SMTPSecure = "ssl";  // ssl required 
 
          $mail->SetFrom($from, $from_name);
          $mail->AddReplyTo($reply_to, $reply_to_name);
          $mail->AddAddress($to, $to_name);
          $mail->Subject    = $subject;
          $mail->MsgHTML($body);
 
          $mail->Send();
     } 
     catch (phpmailerException $e) {
        echo $e->errorMessage();  //Pretty error messages from PHPMailer
     } 
     catch (Exception $e) {
          echo $e->getMessage();  //Boring error messages from anything else!
     }
}
?>