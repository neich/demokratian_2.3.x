<?php
include('verifica.php');
//$nivel_acceso=11; if ($nivel_acceso <= $_SESSION['usuario_nivel']){
if (empty($_SESSION['numero_vot'])) {
    header("Location: $redir?error_login=5");
    exit;
}
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><?php echo "$nombre_web"; ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content=" ">
        <meta name="author" content=" ">
        <link rel="icon"  type="image/png"  href="../temas/<?php echo "$tema_web"; ?>/imagenes/icono.png"> 



        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../temas/<?php echo "$tema_web"; ?>/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

        <link href="../temas/<?php echo "$tema_web"; ?>/estilo.css" rel="stylesheet">

    </head>
    <body>

        <div class="container">

            <!-- cabecera
            ================================================== -->
            <div class="page-header">
                <img src="../temas/<?php echo "$tema_web"; ?>/imagenes/cabecera_votaciones.jpg" class="img-responsive" alt="Logo <?php echo "$nombre_web"; ?>">
            </div>

            <!-- END cabecera
            ================================================== -->


            <div class="row">




                <div class="col-md-2" >             

                    <a href="log_out.php">SALIR</a>

                </div>



                <div class="col-md-10">

                    <!--Comiezo-->

                    <h1> Ha accedido a la votación <strong>" <?php echo $_SESSION['nombre_votacion']; ?> "</strong> para generar sus claves publica y privada.</h1>
                    <h3><span class="label label-warning"> Recuerde que es sumamente importante que guarde sus claves y realice correctamente el proceso</span></h3>



                    <?php
                    include('../modulos/phpseclib/Crypt/RSA.php');
//include('../modulos/phpseclib/Net/SSH2.php');
                    $rsa = new Crypt_RSA();
//$rsa -> setPassword('keypwd');
//$rsa->setPrivateKeyFormat(CRYPT_RSA_PRIVATE_FORMAT_PKCS1);
//$rsa->setPublicKeyFormat(CRYPT_RSA_PUBLIC_FORMAT_PKCS1);
//define('CRYPT_RSA_EXPONENT', 65537);
//define('CRYPT_RSA_SMALLEST_PRIME', 64); // makes it so multi-prime RSA is used
                    extract($rsa->createKey()); // == $rsa->createKey(1024) where 1024 is the key size
                    echo "<br/>";
                    echo "<br/>";
                    echo $publickey;
                    echo "<br/>";
                    echo "<br/>";
                    echo $privatekey;

//$pK = bin2hex($privatekey);
//echo $pK;
                    ?>


                    <form name="form1" method="post" action="descarga_clave.php">

                        <div class="form-group">       
                            <label for="nombre" class="col-sm-3 control-label"> Tu clave publica</label>

                            <div class="col-sm-9">
                                <textarea name="clave_publica" rows="7" autofocus required class="form-control" id="clave_publica"><?php echo "$publickey"; ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">       
                            <label for="nombre" class="col-sm-3 control-label"> Tu clave privada</label>

                            <div class="col-sm-9">
                                <textarea name="clave_privada" rows="15" autofocus required class="form-control" id="clave_privada"><?php echo "$privatekey"; ?></textarea>
                            </div>
                        </div>
                        <p>&nbsp;</p><p>&nbsp;</p>
                        <div class="col-sm-2">
                        </div>
                        <div class="col-sm-7">
                            <h4><span class="label label-warning">Al presionar el botón de guardar claves, se descargara un archivo llamado claves.txt</span></h4> 
                            <h4><span class="label label-warning">Guardelo en su ordenador y no la pierda, sino la votacion no podra ser desencriptada.</span></h4>
                        </div>
                        <div class="col-sm-3">
                            <input name="add_clave" type=submit class="btn btn-primary pull-right"  id="add_clave" value="Guardar claves" />

                        </div>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                    </form>

                    <!--Final-->

                </div>



            </div>


            <div id="footer" class="row">
                <!--
            ===========================  modal para apuntarse
                -->
                <div class="modal fade" id="apuntarme" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-body"></div>

                        </div> <!-- /.modal-content -->
                    </div> <!-- /.modal-dialog -->
                </div> <!-- /.modal -->

                <!--
               ===========================  FIN modal apuntarse
                -->

                <?php include("../temas/$tema_web/pie.php"); ?>
            </div>
        </div>  


 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->    
        <script src="../js/jquery-1.9.0.min.js"></script>
        <script src="../modulos/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            function loadAcceso() {
                //Funcion para cargar el muro
                $("#acceso").load('acceso.php?idvot=<?php echo ($_SESSION['numero_vot']); ?>');
                //Devuelve el campo message a vacio
                // $("#msg").val("")
                //var idvot = $("#idvot").val();
            }

        </script>

        <?php
        if ($_SESSION['numero_inter'] == $contador) {
            //echo "Ya estan validados todos los interventores para esta votación<br/>";
            ?>
            <script type="text/javascript">
                $(document).ready(function() {
                    loadAcceso();
                });
            </script>
            <?php
        }
        ?>

                                <!--<script type="text/javascript">
                                $(document).ready(function(){
                        loadAcceso(); 
                                 });
                                 </script>-->

    </body>
</html>