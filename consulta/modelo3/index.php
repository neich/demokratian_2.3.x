 
 <div class="row bootstrap snippets" id="fondo">
      <?php  
	   $numcolumnas=$aux; //demomento el dato aux solo lleva el dato del numero de columnas, pero en el futuro sera un array de datos,
       $n_letras = 50;
      // $numcolumnas = 4; 
		if($numcolumnas==2){
			$class_columnas="col-xs-12 col-sm-6 col-md-6";
		}else if($numcolumnas==3){
			$class_columnas="col-xs-12 col-sm-4 col-md-4";			
		}else if($numcolumnas==4){
			$class_columnas="col-xs-12 col-sm-4 col-md-3";			
		}else if($numcolumnas==6){
			$class_columnas="col-xs-12 col-sm-4 col-md-2";	//		
		}else if($numcolumnas==12){
			$class_columnas="col-xs-6 col-sm-3 col-md-1";			
		}
		
		
		$data="shrink"; //gray, vertpan, tilt,  shrink 
        if ($total_resultados>0) {  
			 $i = 1; 
			 while($row = mysqli_fetch_array($result)){ 
			   $resto = ($i % $numcolumnas);  
			   if($resto == 1){ /*si es el primer elemento creamos una nueva fila*/  
				 ?>
				 <div class="row">
				 <?php 
			   } ?>      
               		<div class="<?php echo $class_columnas; ?> <?php echo $data;?> ">
                      <div class="caja_ficha">
                        <h4 class="category centrado"><?php echo $row['nombre_usuario'];?></h4>             
                        <p  class="centrado"><a data-toggle="modal"  href="../votacion/perfil.php?idgr=<?php echo $row[0]; ?>" data-target="#ayuda_contacta" title="<?php echo $row['nombre_usuario']; ?>"  > <?php if ($row[imagen_pequena] == "") { ?><img src="../temas/<?php echo "$tema_web"; ?>/imagenes/avatar_sin_imagen.jpg" width="110" height="110" class="borde" /><?php } else { ?><img src="<?php echo $upload_cat; ?>/<?php echo $row['imagen_pequena']; ?>" alt="<?php echo $row['nombre_usuario']; ?> " width="110" height="110"  class="borde"  /> <?php } ?></a></p>
                        <p class="description">
						<?php 
							$txt=strip_tags($row['texto']);
							$txt= mb_substr($txt,0,$n_letras,'UTF-8'); 
							echo $txt;						
						?>
                        </p>
                        <h6 class="title centrado"><a data-toggle="modal"  href="../votacion/perfil.php?idgr=<?php echo $row[0]; ?>" data-target="#ayuda_contacta" title="<?php echo $row['nombre_usuario']; ?>"  >más información</a></h6>
               		 </div>
                    </div>
               
 		   
					
		   <?php  if($resto == 0){ 
			  /*cerramos la fila*/  
			  ?>
			  </div>
			  <?php 
			   } 
			  $i++;  
			 } 
			
			 if($resto != 0){ 
			  /*Si en la &uacute;ltima fila sobran columnas, creamos celdas vac&iacute;as*/ 
			   for ($j = 0; $j < ($numcolumnas - $resto); $j++){ 
				 echo "<div class=\"col-md-3\"></div>";  
				} 
			   echo "</tr>"; 
			  }  
			}

		?>  
   </div>
