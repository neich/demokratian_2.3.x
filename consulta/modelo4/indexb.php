
  <div class="row bootstrap  demo-2 no-js" id="fondo">  
       <?php  
	   $numcolumnas=$aux; //demomento el dato aux solo lleva el dato del numero de columnas, pero en el futuro sera un array de datos,
       $n_letras = 80;
      // $numcolumnas = 4; 
		if($numcolumnas==2){
			$class_columnas="col-xs-12 col-sm-6 col-md-6";
			$tam =450;//
		}else if($numcolumnas==3){
			$class_columnas="col-xs-12 col-sm-4 col-md-4";
			$tam =350;//			
		}else if($numcolumnas==4){
			$tam =250;//
			$class_columnas="col-xs-12 col-sm-4 col-md-3";			
		}else if($numcolumnas==6){
			$class_columnas="col-xs-12 col-sm-4 col-md-2";	
			$tam =150;//		
		}else if($numcolumnas==12){
			$class_columnas="col-xs-6 col-sm-3 col-md-1";	
			$tam =50;//		
		}
		

        if ($total_resultados>0) {  
			 $i = 1; 
			 while($row = mysqli_fetch_array($result)){ 
			   $resto = ($i % $numcolumnas);  
			   if($resto == 1){ /*si es el primer elemento creamos una nueva fila*/  
				 ?>
				 <div class="row">
				 <?php 
			   } ?>  
                  
        <div class="<?php echo $class_columnas; ?>  caja_ficha">
			<!-- Top Navigation -->
			
            
            
			<section id="grid" class="grid clearfix">
				<a href="#" data-path-hover="m 0,0 0,47.7775 c 24.580441,3.12569 55.897012,-8.199417 90,-8.199417 34.10299,0 65.41956,11.325107 90,8.199417 L 180,0 z">
					<figure>
						<?php if ($row[imagen_pequena] == "") { ?><img src="../temas/<?php echo "$tema_web"; ?>/imagenes/avatar_sin_imagen.jpg" width="<?php echo "$tam"; ?>" height="<?php echo "$tam"; ?>"  /><?php } else { ?><img src="<?php echo $upload_cat; ?>/<?php echo $row['imagen_pequena']; ?>" alt="<?php echo $row['nombre_usuario']; ?> " width="<?php echo "$tam"; ?>" height="<?php echo "$tam"; ?>"  class="borde"  /> <?php } ?>
						<svg viewBox="0 0 180 320" preserveAspectRatio="none"><path d="m 0,0 0,171.14385 c 24.580441,15.47138 55.897012,24.75772 90,24.75772 34.10299,0 65.41956,-9.28634 90,-24.75772 L 180,0 0,0 z"/></svg>
						<figcaption>
							<h2><?php echo $row['nombre_usuario'];?> </h2>
							<p>	
                            <?php 
							$txt=strip_tags($row['texto']);
							$txt= mb_substr($txt,0,$n_letras,'UTF-8'); 
							echo $txt;						
							?></p>
							<button type="button" data-toggle="modal" data-target="#ayuda_contacta" href="../votacion/perfil.php?idgr=<?php echo $row[0]; ?>">más información</button>
						</figcaption>
					</figure>
				</a>

			</section>

		</div><!-- /container -->
        
        		   <?php  if($resto == 0){ 
			  /*cerramos la fila*/  
			  ?>
			  </div>
			  <?php 
			   } 
			  $i++;  
			 } 
			
			 if($resto != 0){ 
			  /*Si en la &uacute;ltima fila sobran columnas, creamos celdas vac&iacute;as*/ 
			   for ($j = 0; $j < ($numcolumnas - $resto); $j++){ 
				 echo "<div class=\"col-md-3\"></div>";  
				} 
			   echo "</div>"; 
			  }  
			}

		?>  
   
   </div>
		<script>
			(function() {
	
				function init() {
					var speed = 330,
						easing = mina.backout;

					[].slice.call ( document.querySelectorAll( '#grid > a' ) ).forEach( function( el ) {
						var s = Snap( el.querySelector( 'svg' ) ), path = s.select( 'path' ),
							pathConfig = {
								from : path.attr( 'd' ),
								to : el.getAttribute( 'data-path-hover' )
							};

						el.addEventListener( 'mouseenter', function() {
							path.animate( { 'path' : pathConfig.to }, speed, easing );
						} );

						el.addEventListener( 'mouseleave', function() {
							path.animate( { 'path' : pathConfig.from }, speed, easing );
						} );
					} );
				}

				init();

			})();
		</script>
