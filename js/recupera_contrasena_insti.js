// JavaScript Document

$(function() {
    $("#contrasenaFormInsti").find("input,select").jqBootstrapValidation(// este seria con un formulario con class="form-horizontal"
            //$("input#name,input#email,select#provincia").jqBootstrapValidation(  //  ver que ese texarea sera un select
                    {
                        preventSubmit: true,
                        submitError: function($form, event, errors) {
                            // something to have when submit produces an error ?
                            // Not decided if I need it yet
                        },
                        submitSuccess: function($form, event) {
                            event.preventDefault(); // prevent default submit behaviour
                            // get values from FORM
                           var dataString = $("#contrasenaFormInsti").serialize();

                            $.ajax({
                                url: "basicos_php/procesar_insti.php",
                                type: "POST",
                                data: dataString,
                                cache: false,
                                success: function(data) {
                                    // Success message
                                   //$('#success').html(" " + data + " ");
                                    
                                    var result = data.trim().split("##");
                                    if (result[0] == 'OK') {
                                        $("#divcontrasenaFormInsti").hide("slow");
                                        $('#success').html(result[1]);
                                        $('#success').show();
                                        $('#contactForm').trigger("reset");
                                    } else {

                                        $("#success").show();
                                        $('#success').html(result[1]);
                                    }
                                },
                               error: function() {		
// 				// Fail message
 		 		$('#success').html("<div class='alert alert-danger'>");
                 		$('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                      	      .append(" " +contrasenaFormInsti+" , hay uns error</button>");


	   			 }
                            })
                        },
                        filter: function() {
                            return $(this).is(":visible");
                        },
                    });
        });


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});