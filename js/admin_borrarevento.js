
// JavaScript Document

function borrarevento()
{
    if (confirm('Antes de continuar   \n  ¿esta Ud. seguro de querer borrar este registro?.  \n  ¡ya no se podra recuperar! \n\n  Si lo borra y la votacion esta en marcha o ya se ha realizado puede crear errores inesperados\n\n'))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function borravotante()
{
    if (confirm('Antes de continuar   \n  ¿esta Ud. seguro de querer borrar este votante?.  \n  ¡ya no se podra recuperar! \n\n  Si lo borra  puede crear errores inesperados en las votaciones donde ha participado este usuario\n\n'))
    {
        return true;
    }
    else
    {
        return false;
    }
}