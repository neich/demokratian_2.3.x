﻿# README #

Los requerimientos necesarios para poner en marcha la plataforma de votaciones DEMOKRATIAN son muy sencillos. Necesita un servidor apache con php5 y una base de datos mysql. Dependiendo de si tiene muchos miles de inscritos el servidor tendrá que ser más o menos potente.

##Donar##

Si quieres ayudarnos a mantener el proyecto, puedes hacer una donación.
[DONAR con PAYPAL](https://www.paypal.com/es/cgi-bin/webscr?cmd=_flow&SESSION=G280Mr4zfk_Mu2z08eHScwjVKwOMOIKCj1FR52hT3mkvR8W3awvc9PFT4li&dispatch=5885d80a13c0db1f8e263663d3faee8d64ad11bbf4d2a5a1a0d303a50933f9b2)

También puedes donar tu tiempo y participar en el desarrollo de DEMOKRATIAN, contacta con nosotros info@demokratian.org

### ¿Para que es este repositorio? ###

* DEMOKRATIAN , una plataforma desarrollada por [Carlos Salgado Werner](http://carlos-salgado.es) para poder implementar una forma de decisión horizontal en su organización. Todos los miembros registrados podrán votar de una forma sencilla con la seguridad de que su voto es secreto.
Permite múltiples tipos de votación, actualmente dispone de 4 tipos o formas de votación. De esta forma el administrador puede elegir entre abrir un debate, realizar una encuesta, hacer una votación con voto ponderado, o realizar una votación con recuento por VUT.
DEMOKRATIAN está especialmente diseñada para que sea muy fácil de usar, tanto por el usuario final, el votante, como por los distintos niveles de administradores. De esta forma no tendrá que preocuparse más que de realizar las preguntas o añadir sus candidatos para sus votaciones.
* Version [demokratian_2.3.x] (https://bitbucket.org/csalgadow/demokratian_2.3.x/)
* Web de [demokratian.org](http://demokratian.org)


### Como configurar la aplicación ###

Para instalar la aplicación, suba todos los archivos a su servidor.
Hay 4 carpetas que tienen que tener permiso de escritura para el usuario con el que corre apache: config, upload_user, upload_pic, data_vut (usamos chown $USUARIO.$GRUPO y chmod 0700 para ello). Estas carpetas puede cambiarlas el nombre posteriormente mediante el archivo de configuración.

La aplicación tiene un sistema de instalación automático, para ello debe de ir mediante su navegador a la carpeta install (midominio.org/install)
Para realizar la instalación necesita conocer los datos de la base de datos de su servidor asi como los datos de configuración del servidor de correo.
Esa información se usa para configurar el archivo config.inc.php que está en la carpeta config. Si por cualquier motivo la creación automática no funcionara, o posteriormente quiere hacer una modificación que no pueda realizar mediante el panel de administración de la aplicación, siempre podrá modificar ese archivo a mano.
Es posible que si su servidor tiene poca memoria asignada, o el tiempo de ejecución de PHP es escaso, no funcione la instalación manual, en ese caso, si no puede cambiarla, tendrá que hacer una instalación manual (https://bitbucket.org/csalgadow/demokratian_2.3.x/wiki/Instalaci%C3%B3n)

Si usa una base de datos compartida con otras aplicaciones, es muy recomendable hacer una copia de seguridad de la misma antes de proceder a la instalación. Durante la misma se pueden producir errores inesperados y perder datos. Demokratian no se responsabiliza de posibles bugs o errores que puedan generar perdida de datos.  

### Como contribuir ###

* Writing tests
* Code review
* Other guidelines

### Contactar ###

Para contactar puede hacerlo mediante el correo info@demokratian.org
Aun no tenemos equipo de contribuidores así que  no podemos darle más formas de contacto.

### Contribuidores ###
* En el código:
-> Antonio Garcia (rotobator[AT]gmail.com)