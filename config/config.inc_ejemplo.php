<?php

###############################################################################################################################################################
###############################################################################################################################################################
###                                                                                                                                                         ###
###                                                                DEMOKRATIAN                                                                              ###
###                                                         http://demokratian.org                                                                          ###
###                                                  2015 CARLOS SALGADO WERNER (http://carlos-salgado.es)                                                  ###
###                                         Este programa ha sido creado por Carlos Salgado Werner                                                          ###
###                                                                                                                                                         ###
### Este programa es software libre. Puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General de GNU según es publicada por la ###
### Free Software Foundation, bien de la versión 3 de dicha Licencia o bien de cualquier versión posterior.                                                 ###
### Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA, incluso sin la garantía MERCANTIL implícita o sin garantizar   ###
### la CONVENIENCIA PARA UN PROPÓSITO PARTICULAR. Véase la Licencia Pública General de GNU para más detalles.                                               ###
### Debería haber recibido una copia de la Licencia Pública General junto con este programa. Si no ha sido así, puede encontrarla en                        ###
### http://www.gnu.org/licenses/gpl-3.0.html                                                                                                                ###
### Si quieres participar en la mejora de este software ,eres libre de hacerlo,                                                                             ###
### También puedes contactar con migo en el correo info@demokratian.org para trabajar en el desarrollo de forma colaborativa                                ###
###                                                                                                                                                         ###
###                                          Por favor, no elimines este aviso de licencia                                                                  ###
###                                                                                                                                                         ###
###############################################################################################################################################################
###############################################################################################################################################################
###############################################################################################################################################################
###############################################################################################################################################################
##########                                                                                                                                           ##########
##########                                                 CONFIGURACIÓN del la PLATAFORMA DE VOTACIONES                                             ##########  
##########                                                   Software creado por Carlos Salgado                                                      ##########  
##########                                                                                                                                           ##########
###############################################################################################################################################################
###################################################################################################################################

$hostu = "usuario";                              // Usario de la BBDD
$hostp = "password";                          // Contraseña de la BBDD
$dbn = "votaciones_demokratian";              // Nombre de la BBDD
$host = "host";                            // localhost de la BBDD                  		  					 				 
$extension="dk_";                             // prefijo de las tablas de la base de datos

$url_vot="http://localhost/pruebas_2.2.x";      // Url donde instalamos nuestra aplicación

$nombre_web="DEMOKRATIAN |  Centro de votaciones";    // Nombre del sitio web
$tema_web="demokratian";                       // Nombre del tema (carpeta donde se encuentra)
$info_versiones=true;         //sistema de avisos de nuevas versiones, por defecto habilitado (true)
###################################################################################################################################
##########                          Direcciones de correo para los distintos tipos de envios                             ##########  
###################################################################################################################################


$email_env="info1@demokratian.org"; //direccion de correo general
$email_error="info2@demokratian.org"; //sitio al que enviamos los correos de los que tienen problemas y no estan en la bbdd, 
//este correo es el usado si no hay datos en la bbdd de los contactos por provincias
$email_control="info3@demokratian.org"; //Direccion que envia el correo para el control con interventores

$email_error_tecnico="info4@demokratian.org"; //correo electronico del responsable tecnico
$email_sistema="info5@demokratian.org"; //correo electronico del sistema, demomento incluido en el envio de errores de la bbdd

$asunto_mens_error="root de votaciones con problemas"; //asunto del mensaje de correo cuando hay problemas de acceso
$nombre_eq="Votaciones DEMOKRATIAN"; //asunto del correo


$nombre_sistema="Sistema de  votaciones"; // Nombre del sistema cuando se envia el correo de recupercion de clave
$asunto="Recuperar tu contraseña en DEMOKRATIAN"; // asunto para recuperar la contraseña
###################################################################################################################################
#############         Configuracion del correo smtp , solo si es tenemos como true la variable $correo_smtp           #############
###################################################################################################################################
$correo_smtp=true;                   //poner en false si queremos que el envio se realice con phpmail() y true si es por smtp

$user_mail="info";        // root de correo
$pass_mail="22222222";  //  de correo
$localhost_smtp="smtp.demokratian.org";        // localhost del correo  smtp.1and1.es
$puerto_mail=25;
$mail_sendmail=true; // en algunos sevidores como 1¬1 hay que usar IsSendMail() en vez de IsSMTP() por defecto dejar en false
###################################################################################################################################
#############                   CONFIGURACIÓN para la conexion con el CIVICRM para gestion de  roots                  #############
#############                                      No funciona aun, en construccion                                   #############
###################################################################################################################################
//$civi=true;  ////poner false si no queremos que conecte  con civicrm
###################################################################################################################################
#############                       Todo este grupo de variables no tienen porque ser modificadas.                    #############
#############                                 hacerlo solo si se tiene conocimientos                                  #############
###################################################################################################################################

$b_debugmode=0; // 0 || 1  Forma de errores cuando hay problemas con la base de datos
##################################################################################################################################
###############                                        configuración de carpetas                                   ###############
###############                    Ojo, darle permisos de escritura en el servidor si corre en Linux               ###############
##################################################################################################################################

$FilePath="../data_vut/";                  //   carpeta donde se generan los archivos del vut
$path_bakup_bbdd="../admin/backup";                 // Carpeta donde se guardan los back-up de la bbdd
##################################################################################################################################
#####################                 Otras viariables del sistema, no son necesario cambiarlas              #####################
##################################################################################################################################


$usuarios_sesion="aut_usu_ad"; // nombre de la sesion 
$usuarios_sesion2="ses_inter_admi"; // nombre de la sesion de los interventores
$clave_encriptacion="aoleequo33892"; // 
$clave_encriptacion2="aoleequo33892";



##################################################################################################################################
###############     variables del sistema de subida y/o redimension de imagenes de los candidatos y roots       ###############
###############                    Ojo, darle permisos de escritura en el servidor si corre en Linux               ###############
##################################################################################################################################

$upload_cat = "../upload_pic"; //carpeta donde se guardan las imagenes de los candidatos
$upload_user = "../upload_user"; //carpeta donde se guardan las imagenes de los roots
$baseUrl = $url_vot . '/userfile/'; //   carpeta donde se guardan las imagenes y archivos de gestor ckfinder

###################################################################################################################################
##########                                 Configuración para uso municipal                                              ##########  
###################################################################################################################################	
$es_municipal=false;  // true si vamos a trabajar solo con un tipo de circunscripcion y no queremos que pregunte la provincia. 
                      //Si se usa la opción de correos institucionales debe de ser true

###################################################################################################################################
##########              Configuración para uso de correos institucionales de empresas u organizaciones                   ##########  
###################################################################################################################################	
$insti=false; /////si usa el sistema de inscripcion de correos instirucionales, por defecto false
$term_mail=""; ///// terminacion del correo institucional incluyendo la arroba, por defecto vacio
$term_empre=""; //// Nombre de la empresa

###################################################################################################################################
##########                                               Autenticacion                                                   ##########  
###################################################################################################################################								
$cfg_autenticacion_solo_local=false;    // ¿Permitimos autenticacion federada? cfg_autenticacion_solo_local==false -> SI
$cfg_dir_simplesaml="var";                    // Directorio donde hemos instalado simplesamlphp en modo SP.
$cfg_crear_usuarios_automaticamente=true;    // Cuando tenemos autenticacion federada, ¿creamos usuarios automaticamente?
$cfg_tipo_usuario=1;    // Al crear automaticamente, el tipo por defecto


?>
